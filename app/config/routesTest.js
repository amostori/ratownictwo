import { createStackNavigator } from 'react-navigation';
import React from 'react';
import { Linking } from 'react-native';
import Home from '../screens/Home';
import {
  AlsScreen,
  PalsScreen,
  PoloznictwoScreen,
  UrazyScreen,
  ZatruciaScreen,
} from '../screens/ListScreens';
import {
  EnergiaDefibrylacji,
  Screen4H4T,
  Ogrzewanie,
  KlasyfikacjaHipotermii,
  Sedacja,
  Kardiowersja,
  AdrenalinaWskazowki,
  AmiodaronWskazowki,
  SaturacjaSwiezorodka,
  AtropinaWskazowki,
  SedacjaDzieci,
  MorfinaDzieci,
  SkalaBolu,
  DiazepamDzieci,
  MidazolamDzieci,
  LeczenieGoraczki,
  Hiperwentylacja,
  Toksydrom,
} from '../screens/wskazowkiScreens';
import {
  Adrenalina,
  Amiodaron,
  Lignokaina,
  Atropina,
  Fentanyl,
  Midazolam,
  Morfina,
  Diazepam,
  Magnez,
  Metoprolol,
  Nitrogliceryna,
  ASA,
  Tikagrelor,
  Klopidogrel,
  Heparyna,
  Urapidil,
  Hydroxyzyna,
  Furosemid,
  Captopril,
  Hydrokortyzon,
  Dexaven,
  Clemastin,
  Glukagon,
  Salbutamol,
  Paracetamol,
  Pyralgina,
  Papaweryna,
  Drotaweryna,
  Glukoza,
  Clonazepam,
  Budezonid,
} from '../screens/farmascreens';
import {
  Wstep,
  FunkcjeZyciowe,
  Zlecenia,
  BadanieKliniczne,
  Podsumowanie,
} from '../screens/wskazowkiScreens/badanieSubScreens';
import {
  BadanieScreen,
  NzkAlsScreen,
  NzkHipotermia,
  Hipotermia,
  Bradykardia,
  Czestoskurcz,
  Ozw,
  Nadcisnienie,
  Dusznosc,
  Obrzek,
  Anafilaksja,
  Astma,
  BolBrzucha,
  Hipoglikemia,
  Hiperglikemia,
  Padaczka,
  Udar,
  BolPlecow,
  ZaburzeniaPsychiczne,
} from '../screens/alsScreens';
import {
  NzkP,
  Swiezorodek,
  BradykardiaP,
  CzestoskurczP,
  AnafilaksjaP,
  AstmaP,
  ZapalenieKrtani,
  BolBrzuchaP,
  HipoglikemiaP,
  DrgawkiP,
  Goraczka,
} from '../screens/palsScreens';
import { FilmsButton } from '../components/Buttons';
import {
  Porod,
  Pepowina,
  PepowinaNaSzyi,
  KrwawieniePorod,
  Rzucawka,
} from '../screens/poloznictwoScreens';
import {
  ZdarzenieMasowe,
  TriageAdult,
  TriageJump,
  CentrumUrazowe,
  Czaszka,
} from '../screens/urazyScreens';

import { Amfetamina, Benzodiazepiny } from '../screens/zatruciaScreens';

export default createStackNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        headerTitle: 'Ratownictwo medyczne',
      },
    },
    AlsScreen: {
      screen: AlsScreen,
      navigationOptions: {
        headerTitle: 'ALS',
      },
    },
    ZatruciaScreen: {
      screen: ZatruciaScreen,
      navigationOptions: {
        headerTitle: 'Zatrucia',
      },
    },
    ZdarzenieMasowe: {
      screen: ZdarzenieMasowe,
      navigationOptions: {
        headerTitle: 'Zdarzenie Masowe',
      },
    },
    Toksydrom: {
      screen: Toksydrom,
      navigationOptions: {
        headerTitle: 'Toksydromy',
      },
    },
    Amfetamina: {
      screen: Amfetamina,
      navigationOptions: {
        headerTitle: 'Amfetamina, Kokaina',
      },
    },
    Benzodiazepiny: {
      screen: Benzodiazepiny,
      navigationOptions: {
        headerTitle: 'Benzodiazepiny',
      },
    },
    TriageAdult: {
      screen: TriageAdult,
      navigationOptions: {
        headerTitle: 'TRIAGE',
      },
    },
    TriageJump: {
      screen: TriageJump,
      navigationOptions: {
        headerTitle: 'TRIAGE JUMP',
      },
    },
    CentrumUrazowe: {
      screen: CentrumUrazowe,
      navigationOptions: {
        headerTitle: 'Centrum Urazowe - kryteria przyjęć',
      },
    },
    Anafilaksja: {
      screen: Anafilaksja,
      navigationOptions: {
        headerTitle: 'Anafilaksja',
      },
    },
    SaturacjaSwiezorodka: {
      screen: SaturacjaSwiezorodka,
      navigationOptions: {
        headerTitle: 'Saturacja Świeżorodka',
      },
    },
    Hiperwentylacja: {
      screen: Hiperwentylacja,
      navigationOptions: {
        headerTitle: 'Hiperwentylacja',
      },
    },
    Czaszka: {
      screen: Czaszka,
      navigationOptions: {
        headerTitle: 'Uraz czaszkowo - mózgowy',
      },
    },
    NzkP: {
      screen: NzkP,
      navigationOptions: {
        headerTitle: 'Nagłe zatrzymanie krążenia u dzieci',
        headerRight: (
          <FilmsButton
            showMovie={() => Linking.openURL('https://www.youtube.com/watch?v=WGQMNBiHPP8')}
          />
        ),
      },
    },
    Swiezorodek: {
      screen: Swiezorodek,
      navigationOptions: {
        headerTitle: 'Resuscytacja świeżorodka',
      },
    },
    BradykardiaP: {
      screen: BradykardiaP,
      navigationOptions: {
        headerTitle: 'Bradykardia u dzieci',
        headerRight: (
          <FilmsButton
            showMovie={() => Linking.openURL('https://www.youtube.com/watch?v=1fjmyog37Fo')}
          />
        ),
      },
    },
    CzestoskurczP: {
      screen: CzestoskurczP,
      navigationOptions: {
        headerTitle: 'Częstoskurcz u dzieci',
        headerRight: (
          <FilmsButton
            showMovie={() => Linking.openURL('https://www.youtube.com/watch?v=AJY2yoQUqzA')}
          />
        ),
      },
    },
    ZapalenieKrtani: {
      screen: ZapalenieKrtani,
      navigationOptions: {
        headerTitle: 'Zapalenie krtani u dzieci',
        headerRight: (
          <FilmsButton
            showMovie={() => Linking.openURL('https://www.youtube.com/watch?v=d8k-4GI429o')}
          />
        ),
      },
    },
    DrgawkiP: {
      screen: DrgawkiP,
      navigationOptions: {
        headerTitle: 'Drgawki u dzieci',
        headerRight: (
          <FilmsButton
            showMovie={() => Linking.openURL('https://www.youtube.com/watch?v=Ac17vTPGbl0')}
          />
        ),
      },
    },
    DiazepamDzieci: {
      screen: DiazepamDzieci,
      navigationOptions: {
        headerTitle: 'Diazepam u dzieci',
      },
    },
    LeczenieGoraczki: {
      screen: LeczenieGoraczki,
      navigationOptions: {
        headerTitle: 'Zwalczanie gorączki u dzieci',
      },
    },
    MidazolamDzieci: {
      screen: MidazolamDzieci,
      navigationOptions: {
        headerTitle: 'Midazolam u dzieci',
      },
    },
    AnafilaksjaP: {
      screen: AnafilaksjaP,
      navigationOptions: {
        headerTitle: 'Anafilaksja u dzieci',
      },
    },
    BolBrzuchaP: {
      screen: BolBrzuchaP,
      navigationOptions: {
        headerTitle: 'Ból brzucha u dzieci',
      },
    },
    HipoglikemiaP: {
      screen: HipoglikemiaP,
      navigationOptions: {
        headerTitle: 'Hipoglikemia u dzieci',
      },
    },
    MorfinaDzieci: {
      screen: MorfinaDzieci,
      navigationOptions: {
        headerTitle: 'Morfina u dzieci',
      },
    },
    SkalaBolu: {
      screen: SkalaBolu,
      navigationOptions: {
        headerTitle: 'Obrazkowa skala bólu',
      },
    },
    Budezonid: {
      screen: Budezonid,
      navigationOptions: {
        headerTitle: 'Budezonid',
      },
    },
    AtropinaWskazowki: {
      screen: AtropinaWskazowki,
      navigationOptions: {
        headerTitle: 'Atropina u dzieci',
      },
    },
    SedacjaDzieci: {
      screen: SedacjaDzieci,
      navigationOptions: {
        headerTitle: 'Sedacja u dzieci',
      },
    },
    AstmaP: {
      screen: AstmaP,
      navigationOptions: {
        headerTitle: 'Astma u dzieci',
        headerRight: (
          <FilmsButton
            showMovie={() => Linking.openURL('https://www.youtube.com/watch?v=Ac17vTPGbl0')}
          />
        ),
      },
    },
    Goraczka: {
      screen: Goraczka,
      navigationOptions: {
        headerTitle: 'Gorączka u dzieci',
        headerRight: (
          <FilmsButton
            showMovie={() => Linking.openURL('https://www.youtube.com/watch?v=bj2CNkjvY4o')}
          />
        ),
      },
    },
    Astma: {
      screen: Astma,
      navigationOptions: {
        headerTitle: 'Astma i POCHP',
      },
    },
    AdrenalinaWskazowki: {
      screen: AdrenalinaWskazowki,
      navigationOptions: {
        headerTitle: 'Adrenalina u dzieci',
      },
    },
    AmiodaronWskazowki: {
      screen: AmiodaronWskazowki,
      navigationOptions: {
        headerTitle: 'Amiodaron u dzieci',
      },
    },
    Padaczka: {
      screen: Padaczka,
      navigationOptions: {
        headerTitle: 'Padaczka',
      },
    },

    Udar: {
      screen: Udar,
      navigationOptions: {
        headerTitle: 'Udar',
      },
    },
    ZaburzeniaPsychiczne: {
      screen: ZaburzeniaPsychiczne,
      navigationOptions: {
        headerTitle: 'Zaburzenia psychiczne',
      },
    },
    BolPlecow: {
      screen: BolPlecow,
      navigationOptions: {
        headerTitle: 'Ból pleców',
      },
    },
    Clonazepam: {
      screen: Clonazepam,
      navigationOptions: {
        headerTitle: 'Clonazepam',
      },
    },
    BolBrzucha: {
      screen: BolBrzucha,
      navigationOptions: {
        headerTitle: 'Ból brzucha',
      },
    },
    Hipoglikemia: {
      screen: Hipoglikemia,
      navigationOptions: {
        headerTitle: 'Hipoglikemia',
      },
    },
    Glukoza: {
      screen: Glukoza,
      navigationOptions: {
        headerTitle: 'Glukoza',
      },
    },
    Hiperglikemia: {
      screen: Hiperglikemia,
      navigationOptions: {
        headerTitle: 'Hiperglikemia',
      },
    },
    Hydrokortyzon: {
      screen: Hydrokortyzon,
      navigationOptions: {
        headerTitle: 'Hydrokortyzon',
      },
    },
    Drotaweryna: {
      screen: Drotaweryna,
      navigationOptions: {
        headerTitle: 'Drotaweryna',
      },
    },
    Salbutamol: {
      screen: Salbutamol,
      navigationOptions: {
        headerTitle: 'Salbutamol',
      },
    },
    Glukagon: {
      screen: Glukagon,
      navigationOptions: {
        headerTitle: 'Glukagon',
      },
    },
    Clemastin: {
      screen: Clemastin,
      navigationOptions: {
        headerTitle: 'Clemastin',
      },
    },
    Dexaven: {
      screen: Dexaven,
      navigationOptions: {
        headerTitle: 'Deksametazon',
      },
    },
    BadanieScreen: {
      screen: BadanieScreen,
      navigationOptions: {
        headerTitle: 'Badanie ratunkowe',
      },
    },
    Nadcisnienie: {
      screen: Nadcisnienie,
      navigationOptions: {
        headerTitle: 'Nadciśnienie tętnicze',
      },
    },
    Wstep: {
      screen: Wstep,
      navigationOptions: {
        headerTitle: 'I Wstęp',
      },
    },
    FunkcjeZyciowe: {
      screen: FunkcjeZyciowe,
      navigationOptions: {
        headerTitle: 'II Funkcje Życiowe',
      },
    },
    Zlecenia: {
      screen: Zlecenia,
      navigationOptions: {
        headerTitle: 'III Zlecenia i wywiad',
      },
    },
    BadanieKliniczne: {
      screen: BadanieKliniczne,
      navigationOptions: {
        headerTitle: 'IV Badanie Kliniczne',
      },
    },
    Podsumowanie: {
      screen: Podsumowanie,
      navigationOptions: {
        headerTitle: 'V Podsumowanie',
      },
    },

    NzkAlsScreen: {
      screen: NzkAlsScreen,
      navigationOptions: {
        headerTitle: 'NZK',
      },
    },
    NzkHipotermia: {
      screen: NzkHipotermia,
      navigationOptions: {
        headerTitle: 'NZK w hipotermii',
      },
    },
    Obrzek: {
      screen: Obrzek,
      navigationOptions: {
        headerTitle: 'Niewydolnosc lewokomorowa - Obrzęk płuc',
      },
    },
    Ogrzewanie: {
      screen: Ogrzewanie,
      navigationOptions: {
        headerTitle: 'Ogrzewanie pozaszpitalne',
      },
    },
    KlasyfikacjaHipotermii: {
      screen: KlasyfikacjaHipotermii,
      navigationOptions: {
        headerTitle: 'Klasyfikacja hipotermii',
      },
    },
    EnergiaDefibrylacji: {
      screen: EnergiaDefibrylacji,
      navigationOptions: {
        headerTitle: 'Energia defibrylacji',
      },
    },
    Adrenalina: {
      screen: Adrenalina,
      navigationOptions: {
        headerTitle: 'Adrenalina',
      },
    },
    Atropina: {
      screen: Atropina,
      navigationOptions: {
        headerTitle: 'Atropina',
      },
    },
    Captopril: {
      screen: Captopril,
      navigationOptions: {
        headerTitle: 'Captopril',
      },
    },
    ASA: {
      screen: ASA,
      navigationOptions: {
        headerTitle: 'Kwas acetylosalicylowy',
      },
    },
    Paracetamol: {
      screen: Paracetamol,
      navigationOptions: {
        headerTitle: 'Paracetamol',
      },
    },
    Tikagrelor: {
      screen: Tikagrelor,
      navigationOptions: {
        headerTitle: 'Tikagrelor',
      },
    },
    Urapidil: {
      screen: Urapidil,
      navigationOptions: {
        headerTitle: 'Urapidil',
      },
    },
    Klopidogrel: {
      screen: Klopidogrel,
      navigationOptions: {
        headerTitle: 'Klopidogrel',
      },
    },
    Heparyna: {
      screen: Heparyna,
      navigationOptions: {
        headerTitle: 'Heparyna',
      },
    },
    Hydroxyzyna: {
      screen: Hydroxyzyna,
      navigationOptions: {
        headerTitle: 'Hydroxyzyna',
      },
    },
    Amiodaron: {
      screen: Amiodaron,
      navigationOptions: {
        headerTitle: 'Amiodaron',
      },
    },
    Furosemid: {
      screen: Furosemid,
      navigationOptions: {
        headerTitle: 'Furosemid',
      },
    },
    Lignokaina: {
      screen: Lignokaina,
      navigationOptions: {
        headerTitle: 'Lignokaina',
      },
    },
    Fentanyl: {
      screen: Fentanyl,
      navigationOptions: {
        headerTitle: 'Fentanyl',
      },
    },
    Midazolam: {
      screen: Midazolam,
      navigationOptions: {
        headerTitle: 'Midazolam',
      },
    },
    Morfina: {
      screen: Morfina,
      navigationOptions: {
        headerTitle: 'Morfina',
      },
    },
    Diazepam: {
      screen: Diazepam,
      navigationOptions: {
        headerTitle: 'Diazepam',
      },
    },
    Metoprolol: {
      screen: Metoprolol,
      navigationOptions: {
        headerTitle: 'Metoprolol',
      },
    },
    Screen4H4T: {
      screen: Screen4H4T,
      navigationOptions: {
        headerTitle: 'Odwracalne przyczyny NZK',
      },
    },
    Hipotermia: {
      screen: Hipotermia,
      navigationOptions: {
        headerTitle: 'Hipotermia',
      },
    },
    Bradykardia: {
      screen: Bradykardia,
      navigationOptions: {
        headerTitle: 'Bradykardia',
      },
    },
    Czestoskurcz: {
      screen: Czestoskurcz,
      navigationOptions: {
        headerTitle: 'Czestoskurcz',
        headerRight: (
          <FilmsButton
            showMovie={() => Linking.openURL('https://www.youtube.com/watch?v=8DIRiOA_OsA')}
          />
        ),
      },
    },
    Ozw: {
      screen: Ozw,
      navigationOptions: {
        headerTitle: 'Ból w klatce piersiowej',
      },
    },
    Dusznosc: {
      screen: Dusznosc,
      navigationOptions: {
        headerTitle: 'Duszność',
      },
    },
    Kardiowersja: {
      screen: Kardiowersja,
      navigationOptions: {
        headerTitle: 'Energia kardiowersji',
      },
    },
    Magnez: {
      screen: Magnez,
      navigationOptions: {
        headerTitle: 'Magnez',
      },
    },
    Nitrogliceryna: {
      screen: Nitrogliceryna,
      navigationOptions: {
        headerTitle: 'Nitrogliceryna',
      },
    },
    Pyralgina: {
      screen: Pyralgina,
      navigationOptions: {
        headerTitle: 'Metamizol - Pyralgina',
      },
    },
    Papaweryna: {
      screen: Papaweryna,
      navigationOptions: {
        headerTitle: 'Papaweryna',
      },
    },
    Sedacja: {
      screen: Sedacja,
      navigationOptions: {
        headerTitle: 'Sedacja i analgezja',
      },
    },
    PalsScreen: {
      screen: PalsScreen,
      navigationOptions: {
        headerTitle: 'PALS',
      },
    },
    PoloznictwoScreen: {
      screen: PoloznictwoScreen,
      navigationOptions: {
        headerTitle: 'Położnictwo',
      },
    },
    UrazyScreen: {
      screen: UrazyScreen,
      navigationOptions: {
        headerTitle: 'Urazy',
      },
    },
    Porod: {
      screen: Porod,
      navigationOptions: {
        headerTitle: 'Poród fizjologiczny',
      },
    },
    Pepowina: {
      screen: Pepowina,
      navigationOptions: {
        headerTitle: 'Wypadnięta pępowina',
      },
    },
    PepowinaNaSzyi: {
      screen: PepowinaNaSzyi,
      navigationOptions: {
        headerTitle: 'Pepowina wokół szyi',
      },
    },
    KrwawieniePorod: {
      screen: KrwawieniePorod,
      navigationOptions: {
        headerTitle: 'Masywne krwawienie po porodzie',
      },
    },
    Rzucawka: {
      screen: Rzucawka,
      navigationOptions: {
        headerTitle: 'Rzucawka porodowa',
      },
    },
  },
  {
    // headerMode: 'none',
    navigationOptions: {
      headerStyle: {
        backgroundColor: '#ff5353',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  },
);
