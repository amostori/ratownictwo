import React from 'react';
import EStyleSheet from 'react-native-extended-stylesheet';
import Navigator from './config/routesTest';
import ZdarzenieMasowe from './screens/urazyScreens/ZdarzenieMasowe';
import TriageAdult from './screens/urazyScreens/triageAdult/TriageAdult';
import TriageJump from './screens/urazyScreens/triageJump/TriageJump';
// import NzkAlsScreen from './screens/alsScreens/NzkAlsScreen';
// import Hipotermia from './screens/alsScreens/hipotermia/blok1';
// import Goraczka from './screens/palsScreens/goraczka/Goraczka';

EStyleSheet.build({
  $primaryBlue: '#4F6D7A',
  $primaryRed: '#ff5353',
  $darkText: '#343434',
  $border: '#e2e2e2',
  $routeBackgroundColor: '#f4511e',
  $routeTintColor: '#fff',
  $badanieButtonBackground: '#ea473f',
  // $outline: 1,
  // outline: 1 powoduje widoczne ramki wokół obiektów
});

export default () => <Navigator />;
