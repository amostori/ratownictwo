import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  container: {
    // nie dawać flex: 1 bo przyciski się skleją
    padding: 10,
    marginVertical: 20,
    alignItems: 'center',
    backgroundColor: '$primaryRed',
    borderRadius: 3,
    width: 120,
  },
  triageButtonContainer: {
    // nie dawać flex: 1 bo przyciski się skleją
    // flex: 1,
    // justifyContent: 'center',
    // padding: 10,
    // marginVertical: 20,
    borderRadius: 3,
    width: 220,
  },
  wrapper: {
    alignItems: 'center',
  },
  tekst: { color: '#ffffff', textAlign: 'center', fontSize: 20 },

  containerYesNo: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  filmButton: {
    marginRight: 20,
  },
  moreFilmButton: {
    marginRight: 20,
    backgroundColor: 'white',
  },
  triageContainer: {
    padding: 10,
    marginVertical: 20,
    alignItems: 'center',
    borderRadius: 3,
    width: 70,
    height: 100,
  },
  triageTekst: {
    borderColor: 'white',
    // borderWidth: 1,
    // borderRadius: 5,
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
    // overflow: 'hidden',
    textAlign: 'center',
    paddingVertical: 20,
  },
  triageColorButtonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
});

export default styles;
