import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const ButtonTriageAdult = ({
  onPress, children, kolor, tekstKolor,
}) => (
  <View style={styles.wrapper}>
    <TouchableOpacity
      style={[styles.triageButtonContainer, { backgroundColor: kolor }]}
      onPress={onPress}
    >
      <Text style={[styles.triageTekst, { color: tekstKolor }]}>{children}</Text>
    </TouchableOpacity>
  </View>
);

ButtonTriageAdult.propTypes = {
  onPress: PropTypes.func,
  children: PropTypes.any,
  kolor: PropTypes.string,
  tekstKolor: PropTypes.string,
};

export default ButtonTriageAdult;
