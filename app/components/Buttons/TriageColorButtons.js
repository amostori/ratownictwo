import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import TriageColorButton from './TriageColorButton';

const TriageColorButtons = ({
  onPressGreen,
  onPressYellow,
  onPressRed,
  onPressBlack,
  greenTekst,
  yellowTekst,
  redTekst,
  blackTekst,
}) => (
  <View style={styles.triageColorButtonsContainer}>
    <TriageColorButton kolor="green" tekstKolor="white" onPress={onPressGreen}>
      {greenTekst}
    </TriageColorButton>
    <TriageColorButton kolor="yellow" tekstKolor="black" onPress={onPressYellow}>
      {yellowTekst}
    </TriageColorButton>
    <TriageColorButton kolor="red" tekstKolor="white" onPress={onPressRed}>
      {redTekst}
    </TriageColorButton>
    <TriageColorButton kolor="black" tekstKolor="white" onPress={onPressBlack}>
      {blackTekst}
    </TriageColorButton>
  </View>
);

TriageColorButtons.propTypes = {
  onPressGreen: PropTypes.func,
  onPressYellow: PropTypes.func,
  onPressRed: PropTypes.func,
  onPressBlack: PropTypes.func,
  greenTekst: PropTypes.any,
  yellowTekst: PropTypes.any,
  redTekst: PropTypes.any,
  blackTekst: PropTypes.any,
};

export default TriageColorButtons;
