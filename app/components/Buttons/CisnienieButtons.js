import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import MyButton from './MyButton';
import styles from './styles';

const CisnienieButtons = ({ onPressBelow, onPressBetween, onPressOver }) => (
  <View style={styles.containerYesNo}>
    <MyButton tekst="&lt;85" onPress={onPressBelow} />
    <MyButton tekst="85-110" onPress={onPressBetween} />
    <MyButton tekst="&gt;110" onPress={onPressOver} />
  </View>
);

CisnienieButtons.propTypes = {
  onPressBelow: PropTypes.func,
  onPressBetween: PropTypes.func,
  onPressOver: PropTypes.func,
};
export default CisnienieButtons;
