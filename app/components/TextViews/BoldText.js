import React from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const BoldText = ({ children }) => (
  <Text style={styles.boldText}>
    {' '}
    {children}
    {' '}
  </Text>
);

BoldText.propTypes = {
  children: PropTypes.any,
};

export default BoldText;
