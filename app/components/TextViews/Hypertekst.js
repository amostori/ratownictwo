import React from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const Hypertekst = ({ onPress, children }) => (
  <Text style={styles.hypertekst} onPress={onPress}>
    {' '}
    {children}
    {' '}
  </Text>
);

Hypertekst.propTypes = {
  onPress: PropTypes.func,
  children: PropTypes.string,
};

export default Hypertekst;
