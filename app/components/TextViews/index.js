import styles from './styles';
import VersionTextView from './VersionTextView';
import Hypertekst from './Hypertekst';
import MyTextView from './MyTextView';
import AskTextView from './AskTextView';
import BoldText from './BoldText';
import BadanieHipertekst from './BadanieHipertekst';
import ListaTextView from './ListaTextView';
import {
  VersionERC,
  VersionSzczeklikERC,
  VersionSzczeklik,
  Sol,
  VersionTkaczyk,
} from './VersionERC';
import TiM from './TiM';

export {
  styles,
  VersionTextView,
  Hypertekst,
  MyTextView,
  AskTextView,
  BoldText,
  BadanieHipertekst,
  ListaTextView,
  VersionERC,
  VersionSzczeklikERC,
  VersionSzczeklik,
  TiM,
  Sol,
  VersionTkaczyk,
};
