import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  container: {},
  versionTekst: {
    color: '$darkText',
    textAlign: 'center',
    fontSize: 15,
    marginTop: 50,
    marginBottom: 20,
    fontStyle: 'italic',
  },
  hypertekstContainer: {
    marginBottom: 20,
  },
  hypertekst: {
    color: '$primaryRed',
    fontSize: 20,
    fontWeight: 'bold',
    lineHeight: 30,
  },
  mainTekst: {
    textAlign: 'center',
    fontSize: 20,
    marginBottom: 20,
    lineHeight: 30,
  },
  askTekst: {
    textAlign: 'center',
    fontSize: 20,
    marginBottom: 20,
    fontWeight: 'bold',
  },
  boldText: {
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
  },
  simpleTekst: {
    textAlign: 'center',
    fontSize: 20,
  },
  badanieContainer: { marginBottom: 30, marginHorizontal: 20 },
  badanieHypertekst: {
    backgroundColor: '$primaryRed',
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 5,
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
    overflow: 'hidden',
    padding: 12,
    textAlign: 'center',
  },
  listaTextView: {
    textAlign: 'center',
    fontSize: 17,
    lineHeight: 25,
    fontStyle: 'italic',
  },
});
