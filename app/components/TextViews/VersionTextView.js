import React from 'react';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const VersionTextView = ({ children }) => (
  <View style={styles.container}>
    <Text style={styles.versionTekst}>{children}</Text>
  </View>
);

VersionTextView.propTypes = {
  children: PropTypes.any,
};

export default VersionTextView;
