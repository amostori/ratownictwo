import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  scrollContainer: {
    flex: 1,
    marginTop: 20,
    marginHorizontal: 10,
    // alignItems: center powoduje wycentrowanie wszystkiego i połączenie guzik
  },
  wskazowkiContainer: {
    flex: 1,
    marginTop: 20,
    marginHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
