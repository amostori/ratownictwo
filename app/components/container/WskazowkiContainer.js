import React from 'react';
import { View, ScrollView } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const WskazowkiContainer = ({ children }) => (
  <ScrollView>
    <View style={styles.wskazowkiContainer}>{children}</View>
  </ScrollView>
);
WskazowkiContainer.propTypes = {
  children: PropTypes.any,
};
export default WskazowkiContainer;
