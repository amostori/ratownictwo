import React from 'react';
import { View, ScrollView } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const ScrollContainer = ({ children }) => (
  <ScrollView>
    <View style={styles.scrollContainer}>{children}</View>
  </ScrollView>
);
ScrollContainer.propTypes = {
  children: PropTypes.any,
};
export default ScrollContainer;
