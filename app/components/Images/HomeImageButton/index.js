import HomeImageButton from './HomeImageButton';
import AllImagesPage from './AllImagesPage';
import styles from './styles';

export { HomeImageButton, AllImagesPage, styles };
// jeśli importuje się coś z tego pliku to musi być w {} nawiasach.
