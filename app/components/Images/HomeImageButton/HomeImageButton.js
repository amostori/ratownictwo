import React from 'react';
import { TouchableOpacity, Image, View } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const HomeImageButton = ({ onPressMe, myImageFunc }) => (
  <View>
    <TouchableOpacity onPress={onPressMe}>
      <Image resizeMode="contain" style={styles.image} source={myImageFunc} />
    </TouchableOpacity>
  </View>
);

HomeImageButton.propTypes = {
  onPressMe: PropTypes.func,
  myImageFunc: PropTypes.number,
};
export default HomeImageButton;
