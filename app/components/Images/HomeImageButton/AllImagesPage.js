import React, { Component } from 'react';
import { View, Linking } from 'react-native';
import PropTypes from 'prop-types';
import HomeImageButton from './HomeImageButton';
import styles from './styles';

export default class AllImagesPage extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  // Uwaga, żeby to zadziałało trzeba  przesłać "notification" w propsach z nadrzędnego
  // componentu, tu z Home.js
  touchEventOnAls = () => {
    const { navigation } = this.props;
    navigation.navigate('AlsScreen');
  };

  touchEventOnPAls = () => {
    const { navigation } = this.props;
    navigation.navigate('PalsScreen');
  };

  touchEventOnciaza = () => {
    const { navigation } = this.props;
    navigation.navigate('PoloznictwoScreen');
  };

  touchEventOncases = () => {
    Linking.openURL('https://www.youtube.com/watch?v=Ac17vTPGbl0');
  };

  touchEventOnFarma = () => {
    const { navigation } = this.props;
    navigation.navigate('PalsScreen');
  };

  touchEventOnUraz = () => {
    const { navigation } = this.props;
    navigation.navigate('UrazyScreen');
  };

  touchEventOnZatrucia = () => {
    const { navigation } = this.props;
    navigation.navigate('ZatruciaScreen');
  };

  render() {
    return (
      <View style={styles.container}>
        <HomeImageButton
          myImageFunc={require('./images/tools.jpg')}
          onPressMe={this.touchEventOnTools}
        />
        <HomeImageButton
          myImageFunc={require('./images/als.jpg')}
          onPressMe={this.touchEventOnAls}
        />
        <HomeImageButton
          myImageFunc={require('./images/pals.jpg')}
          onPressMe={this.touchEventOnPAls}
        />
        <HomeImageButton
          myImageFunc={require('./images/ciaza.jpg')}
          onPressMe={this.touchEventOnciaza}
        />
        <HomeImageButton
          myImageFunc={require('./images/urazy.jpg')}
          onPressMe={this.touchEventOnUraz}
        />

        <HomeImageButton
          myImageFunc={require('./images/zatrucia.jpg')}
          onPressMe={this.touchEventOnZatrucia}
        />
        <HomeImageButton
          myImageFunc={require('./images/farma.jpg')}
          onPressMe={this.touchEventOnFarma}
        />
        <HomeImageButton
          myImageFunc={require('./images/cases.jpg')}
          onPressMe={this.touchEventOncases}
        />
      </View>
    );
  }
}
