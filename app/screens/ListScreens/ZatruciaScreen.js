import React from 'react';
import { View, FlatList, StatusBar } from 'react-native';
import PropTypes from 'prop-types';
import zatruciaList from './data/zatrucia_list';
import { ListItem, Separator } from '../../components/ListViews/ALSList';

class ZatruciaScreen extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  onPressHandler = (item) => {
    const { navigation } = this.props;
    switch (item) {
      case 'Amfetamina i kokaina T 50':
        navigation.navigate('Amfetamina');
        break;
      case 'Benzodiazepiny T 50':
        navigation.navigate('Swiezorodek');
        break;
      case 'Betablokery i Ca - blokery T 50':
        navigation.navigate('Pepowina');
        break;
      case 'Cyjanki T 50':
        navigation.navigate('PepowinaNaSzyi');
        break;
      case 'Digoksyna T 50':
        navigation.navigate('KrwawieniePorod');
        break;
      case 'Dopalacze T 50':
        navigation.navigate('Rzucawka');
        break;
      case 'LSD, grzyby halucynogenne T 50':
        navigation.navigate('Rzucawka');
        break;
      case 'Metanol, Glikol etylenowy T 50':
        navigation.navigate('Rzucawka');
        break;
      case 'Morfina, Heroina, Kodeina T 50':
        navigation.navigate('Rzucawka');
        break;
      case 'Paracetamol T 50':
        navigation.navigate('Rzucawka');
        break;
      case 'Rtęć T 50':
        navigation.navigate('Rzucawka');
        break;
      case 'Tlenek węgla T 50':
        navigation.navigate('Rzucawka');
        break;
      case 'Trójcykliczne leki przeciwdepresyjne T 50':
        navigation.navigate('Rzucawka');
        break;
      case 'Związki fosfoorganiczne T 50':
        navigation.navigate('Rzucawka');
        break;
      case 'Żelazo T 50':
        navigation.navigate('Rzucawka');
        break;
      default:
        alert('dupa');
        break;
    }
  };

  render() {
    return (
      <View>
        <StatusBar translucent={false} barStyle="default" />
        <FlatList
          data={zatruciaList}
          renderItem={({ item }) => (
            <ListItem text={item} onPress={() => this.onPressHandler(item)} />
          )}
          keyExtractor={item => item}
          ItemSeparatorComponent={Separator}
        />
      </View>
    );
  }
}

export default ZatruciaScreen;
