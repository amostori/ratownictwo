import AlsScreen from './AlsScreen';
import PalsScreen from './PalsScreen';
import PoloznictwoScreen from './PoloznictwoScreen';
import UrazyScreen from './UrazyScreen';
import ZatruciaScreen from './ZatruciaScreen';

export {
  AlsScreen, PalsScreen, PoloznictwoScreen, UrazyScreen, ZatruciaScreen,
};
