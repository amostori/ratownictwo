import React from 'react';
import { View, FlatList, StatusBar } from 'react-native';
import PropTypes from 'prop-types';
import poloznictwoList from './data/poloznictwo_list';
import { ListItem, Separator } from '../../components/ListViews/ALSList';

class PoloznictwoScreen extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  onPressHandler = (item) => {
    const { navigation } = this.props;
    switch (item) {
      case 'Poród fizjologiczny O 26':
        navigation.navigate('Porod');
        break;
      case 'NZK świeżorodka I 46':
        navigation.navigate('Swiezorodek');
        break;
      case 'Wypadnięta pępowina O 26':
        navigation.navigate('Pepowina');
        break;
      case 'Pępowina wokół szyi O 26':
        navigation.navigate('PepowinaNaSzyi');
        break;
      case 'Krwawienie po porodzie O 26':
        navigation.navigate('KrwawieniePorod');
        break;
      case 'Rzucawka porodowa O 26':
        navigation.navigate('Rzucawka');
        break;
      default:
        alert('dupa');
        break;
    }
  };

  render() {
    return (
      <View>
        <StatusBar translucent={false} barStyle="default" />
        <FlatList
          data={poloznictwoList}
          renderItem={({ item }) => (
            <ListItem text={item} onPress={() => this.onPressHandler(item)} />
          )}
          keyExtractor={item => item}
          ItemSeparatorComponent={Separator}
        />
      </View>
    );
  }
}

export default PoloznictwoScreen;
