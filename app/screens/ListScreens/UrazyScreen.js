import React from 'react';
import { View, FlatList, StatusBar } from 'react-native';
import PropTypes from 'prop-types';
import urazyList from './data/urazy_list';
import { ListItem, Separator } from '../../components/ListViews/ALSList';

class UrazyScreen extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  onPressHandler = (item) => {
    const { navigation } = this.props;
    switch (item) {
      case 'Zdarzenia masowe':
        navigation.navigate('ZdarzenieMasowe');
        break;
      case 'TRIAGE':
        navigation.navigate('TriageAdult');
        break;
      case 'TRIAGE pediatryczny':
        navigation.navigate('TriageJump');
        break;
      case 'Centrum Urazowe - kryteria przyjęć':
        navigation.navigate('CentrumUrazowe');
        break;
      case 'Oparzenia T 30.0':
        navigation.navigate('KrwawieniePorod');
        break;
      case 'Uraz czaszkowo - mózgowy':
        navigation.navigate('Czaszka');
        break;

      default:
        alert('dupa');
        break;
    }
  };

  render() {
    return (
      <View>
        <StatusBar translucent={false} barStyle="default" />
        <FlatList
          data={urazyList}
          renderItem={({ item }) => (
            <ListItem text={item} onPress={() => this.onPressHandler(item)} />
          )}
          keyExtractor={item => item}
          ItemSeparatorComponent={Separator}
        />
      </View>
    );
  }
}

export default UrazyScreen;
