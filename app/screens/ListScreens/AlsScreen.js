import React from 'react';
import { View, FlatList, StatusBar } from 'react-native';
import PropTypes from 'prop-types';
import alsList from './data/als_list';
import { ListItem, Separator } from '../../components/ListViews/ALSList';

class AlsScreen extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  onPressHandler = (item) => {
    const { navigation } = this.props;
    switch (item) {
      case 'Badanie ratunkowe':
        navigation.navigate('BadanieScreen');
        break;
      case 'Zatrzymanie krążenia I 46':
        navigation.navigate('NzkAlsScreen');
        break;
      case 'NZK w hipotermii I 46':
        navigation.navigate('NzkHipotermia');
        break;
      case 'Hipotermia R 68':
        navigation.navigate('Hipotermia');
        break;
      case 'Bradykardia R 00.0':
        navigation.navigate('Bradykardia');
        break;
      case 'Częstoskurcz R 00.0':
        navigation.navigate('Czestoskurcz');
        break;
      case 'Ból w klatce piersiowej R 07.4':
        navigation.navigate('Ozw');
        break;
      case 'Nadciśnienie tętnicze R 03.0':
        navigation.navigate('Nadcisnienie');
        break;
      case 'Duszność R 06':
        navigation.navigate('Dusznosc');
        break;
      case 'Obrzęk płuc J 81':
        navigation.navigate('Obrzek');
        break;
      case 'Anafilaksja T 78.2':
        navigation.navigate('Anafilaksja');
        break;
      case 'Astma i POCHP (J 46, J 43)':
        navigation.navigate('Astma');
        break;
      case 'Ból brzucha R 10.4':
        navigation.navigate('BolBrzucha');
        break;
      case 'Hipoglikemia E 16.2':
        navigation.navigate('Hipoglikemia');
        break;
      case 'Hiperglikemia E14.8':
        navigation.navigate('Hiperglikemia');
        break;
      case 'Padaczka G 40':
        navigation.navigate('Padaczka');
        break;
      case 'Udar mózgu I 64':
        navigation.navigate('Udar');
        break;
      case 'Ból pleców M 54':
        navigation.navigate('BolPlecow');
        break;
      case 'Zaburzenia psychiczne F 99':
        navigation.navigate('ZaburzeniaPsychiczne');
        break;
      default:
        alert('dupa');
        break;
    }
  };

  render() {
    return (
      <View>
        <StatusBar translucent={false} barStyle="default" />
        <FlatList
          data={alsList}
          renderItem={({ item }) => (
            <ListItem text={item} onPress={() => this.onPressHandler(item)} />
          )}
          keyExtractor={item => item}
          ItemSeparatorComponent={Separator}
        />
      </View>
    );
  }
}

export default AlsScreen;
