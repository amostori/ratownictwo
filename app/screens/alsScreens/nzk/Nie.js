import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import { MyTextView, Hypertekst, VersionERC } from '../../../components/TextViews';

class Nie extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleAdrenalina = () => {
    const { navigation } = this.props;
    // destrukturyzacja props przesłanych z NzkAlsScreen. Jednym z elementów musi być "navigation"
    navigation.navigate('Adrenalina');
  };

  handle4H4T = () => {
    const { navigation } = this.props;
    navigation.navigate('Screen4H4T');
  };

  render() {
    return (
      <View>
        <MyTextView>
          Kontynuuj resuscytację. Jak najszybciej podaj
          <Hypertekst onPress={this.handleAdrenalina}>Adrenalinę</Hypertekst>
1 mg iv lub io.
          Powtarzaj ją co drugą pętlę.
        </MyTextView>
        <MyTextView>Co 2 minuty oceniaj rytm.</MyTextView>
        <MyTextView>
          Rozważ
          <Hypertekst onPress={this.handle4H4T}>4H i 4T</Hypertekst>
        </MyTextView>
        <MyTextView>
          Pamiętaj, że asystolia wymaga potwierdzenia przez sprawdzenie sprzętu, zmianę
          odprowadzenia, położenia łyżek i zwiększenie cechy.
        </MyTextView>
        <VersionERC />
      </View>
    );
  }
}

export default Nie;
