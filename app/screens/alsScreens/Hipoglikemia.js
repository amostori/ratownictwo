import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollContainer } from '../../components/container';
import {
  MyTextView, Hypertekst, TiM, ListaTextView, VersionERC,
} from '../../components/TextViews';

export default class Hipoglikemia extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleGlukagon = () => {
    const { navigation } = this.props;
    navigation.navigate('Glukagon');
  };

  handleGlukoza = () => {
    const { navigation } = this.props;
    navigation.navigate('Glukoza');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <Hypertekst onPress={this.handleGlukoza}>Glukoza 20%</Hypertekst>
1 ml/kg iv
        </MyTextView>
        <MyTextView>lub jeśli brak dojścia dożylnego:</MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleGlukagon}>Glukagon</Hypertekst>
1 mg i.m.
        </MyTextView>
        <MyTextView />
        <ListaTextView>
          Glukagon będzie nieskuteczny u noworodków oraz gdy hipoglikemii towarzyszy insulinoma, guz
          chromochłonny lub przewlekła choroba wątroby.
        </ListaTextView>
        <TiM />
        <VersionERC />
      </ScrollContainer>
    );
  }
}
