import React, { Component } from 'react';
import { View } from 'react-native';
import { ScrollContainer } from '../../../components/container';
import {
  MyTextView, ListaTextView, TiM, VersionTextView,
} from '../../../components/TextViews';
import { ButtonsYesNo } from '../../../components/Buttons';

export default class ZaburzeniaPsychiczne extends Component {
  state = {
    divy: null,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  render() {
    const { divy } = this.state;
    const Przymus = (
      <View>
        <TiM />
        <MyTextView>
          W razie potrzeby zastosuj środki przymusu bezpośredniego. Zespół podstawowy może stosować
          przytrzymanie i unieruchomienie, nie może natomiast podawać leków bez zgody pacjenta.
          Policja i Straż Pożarna jest zobowiązana do udzielenia pomocy zespołowi karetki.
        </MyTextView>
        <MyTextView>
          Natychmiast powiadom dyspozytora o zastosowanych środkach i wypełnij kartę przymusu
          bezpośredniego.
        </MyTextView>
        <VersionTextView>
          Na podstawie ustawy z dnia 19 sierpnia 1994 r. o ochronie zdrowia psychicznego (Dz. U. z
          1994 r. Nr 111 poz. 535 z późniejszymi zmianami) oraz rozporządzenia Ministra Zdrowia z
          dn. 28 czerwca 2012 r. w sprawie stosowania przymusu bezpośredniego oraz dokonywania oceny
          zasadności jego zastosowania (Dz. U. z 2012 r., poz 740).
        </VersionTextView>
      </View>
    );
    const BezPrzymusu = (
      <View>
        <MyTextView>
          Transport do szpitala lub wezwanie lekarza rodzinnego/opieki całodobowej.
        </MyTextView>
        <MyTextView>
          Jeśli pacjent nie wyraża zgody na przejazd do szpitala i nie ma podstaw do zastosowania
          przymusu bezpośredniego zaproponuj wizytę u lekarza rodzinnego.
        </MyTextView>
        <VersionTextView>
          Na podstawie ustawy z dnia 19 sierpnia 1994 r. o ochronie zdrowia psychicznego (Dz. U. z
          1994 r. Nr 111 poz. 535 z późniejszymi zmianami) oraz rozporządzenia Ministra Zdrowia z
          dn. 28 czerwca 2012 r. w sprawie stosowania przymusu bezpośredniego oraz dokonywania oceny
          zasadności jego zastosowania (Dz. U. z 2012 r., poz 740).
        </VersionTextView>
      </View>
    );
    return (
      <ScrollContainer>
        <MyTextView>
          Oceń bezpieczeństwo w miejscu wezwania. W razie zagrożenia wycofaj zespół i wezwij pomoc.
        </MyTextView>
        <MyTextView>Oceń konieczność hospitalizacji bez zgody pacjenta:</MyTextView>
        <MyTextView>
          <ListaTextView>Pacjent zagraża innym?</ListaTextView>
        </MyTextView>
        <MyTextView>
          <ListaTextView>
            Pacjent zagraża sobie (np. skłonności samobójcze, brak logicznego kontaktu)?
          </ListaTextView>
        </MyTextView>
        <MyTextView>
          <ListaTextView>Pacjent niszczy przedmioty w swoim otoczeniu?</ListaTextView>
        </MyTextView>
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(Przymus)}
          onPressNo={() => this.handleOnClick(BezPrzymusu)}
        />
        <MyTextView />
        {divy}
      </ScrollContainer>
    );
  }
}
