import React from 'react';
import { ScrollContainer } from '../../components/container';
import {
  MyTextView,
  BoldText,
  TiM,
  ListaTextView,
  VersionSzczeklikERC,
} from '../../components/TextViews';

const Udar = () => (
  <ScrollContainer>
    <MyTextView>
      Jeśli występują zaburzenia oddychania rozważ wezwanie karetki S lub LPR - może być konieczna
      intubacja.
    </MyTextView>
    <MyTextView>Kontroluj poziom glukozy i ciśnienie tętnicze.</MyTextView>
    <MyTextView>Rozpocznij powolny wlew krystaloidów w celu utrzymania wkłucia.</MyTextView>
    <TiM />
    <ListaTextView>
      W przypadku gdy podejrzewasz udar mózgu obniżanie ciśnienia nie zawsze jest korzystne.
      Postępowanie uzależnione jest od rodzaju udaru:
    </ListaTextView>
    <MyTextView />
    <MyTextView>
      <BoldText>Udar niedokrwienny:</BoldText>
      <ListaTextView>
        Ostrożnie obniżaj ciśnienie jeśli wartość skurczowego przekracza 220 mmHg lub rozkurczowego
        120 mmHg.
      </ListaTextView>
    </MyTextView>
    <MyTextView>
      <BoldText>Udar krwotoczny:</BoldText>
      <ListaTextView>
        Ostrożnie obniżaj gdy ciśnienie skurczowe przekracza 180 mmHg lub rozkurczowe 105 mmHg.
      </ListaTextView>
    </MyTextView>
    <VersionSzczeklikERC />
  </ScrollContainer>
);

export default Udar;
