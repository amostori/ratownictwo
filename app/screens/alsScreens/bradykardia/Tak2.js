import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { ButtonsYesNo } from '../../../components/Buttons';
import { AskTextView, ListaTextView, MyTextView } from '../../../components/TextViews';
import Tak3 from './Tak3';
import Nie3 from './Nie3';

export default class Tak2 extends Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  render() {
    const { navigation } = this.props;
    const { divy } = this.state;

    return (
      <View>
        <AskTextView>Ryzyko asystolii?</AskTextView>
        <ListaTextView>1. Ostatnio przebyty epizod asystolii? 2. Blok AV Mobitz II?</ListaTextView>
        <ListaTextView>2. Blok AV Mobitz II?</ListaTextView>
        <ListaTextView>3. Całkowity blok serca z szerokimi QRS?</ListaTextView>
        <ListaTextView>4. Przerwy komorowe dłuższe niż 3 sekundy?</ListaTextView>
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(<Tak3 navigation={navigation} />)}
          onPressNo={() => this.handleOnClick(<Nie3 navigation={navigation} />)}
        />
        <MyTextView />
        {divy}
      </View>
    );
  }
}
