import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { View } from 'react-native';
import { MyTextView, Hypertekst, VersionERC } from '../../../components/TextViews';

export default class Tak3 extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleAdrenalina = () => {
    const { navigation } = this.props;
    navigation.navigate('Adrenalina');
  };

  handleSedacja = () => {
    const { navigation } = this.props;
    navigation.navigate('Sedacja');
  };

  render() {
    return (
      <View>
        <MyTextView>Przygotowanie do wykonania stymulacji przezskórnej.</MyTextView>
        <MyTextView>
          W międzyczasie podawaj Atropinę do całkowitej dawki 3 mg oraz rozpocznij wlew
          <Hypertekst onPress={this.handleAdrenalina}>Adrenaliny</Hypertekst>
          od 2 do 10 micg/min.
        </MyTextView>
        <MyTextView>Zamonitoruj pacjenta i przyklej elektrody QuickCombo.</MyTextView>
        <MyTextView>
          Zastosuj
          <Hypertekst onPress={this.handleSedacja}>sedację i analgezję</Hypertekst>
        </MyTextView>
        <MyTextView>
          Ustaw częstość stymulacji na około 70/min i zacznij zwiększać energię od 0 do momentu gdy
          zobaczysz, że rytm został przejęty przez stymulator. Dla bezpieczeństwa zwiększ energię o
          około 10 %.
        </MyTextView>
        <VersionERC />
      </View>
    );
  }
}
