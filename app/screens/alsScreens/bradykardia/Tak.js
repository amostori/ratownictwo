import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { ButtonsYesNo } from '../../../components/Buttons';
import { AskTextView, MyTextView, Hypertekst } from '../../../components/TextViews';
import Tak3 from './Tak3';
import Tak2 from './Tak2';

export default class Tak extends Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleAtropina = () => {
    const { navigation } = this.props;
    navigation.navigate('Atropina');
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  render() {
    const { navigation } = this.props;
    const { divy } = this.state;

    return (
      <View>
        <MyTextView>
          Podaj
          <Hypertekst onPress={this.handleAtropina}>Atropinę</Hypertekst>
          0,5 mg iv
        </MyTextView>
        <AskTextView>Poprawa stanu pacjenta?</AskTextView>
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(<Tak2 navigation={navigation} />)}
          onPressNo={() => this.handleOnClick(<Tak3 navigation={navigation} />)}
        />
        <MyTextView />
        {divy}
      </View>
    );
  }
}
