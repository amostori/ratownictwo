import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollContainer } from '../../../components/container';
import { MyTextView, AskTextView } from '../../../components/TextViews';
import { ButtonsYesNo } from '../../../components/Buttons';
import Tak from './Tak';
import Nie from './Nie';

export default class NzkHipotermia extends Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  render() {
    const { divy } = this.state;
    const { navigation } = this.props;
    return (
      <ScrollContainer>
        <MyTextView>Ocena oddechu i krążenia wydłużona do 1 minuty.</MyTextView>
        <MyTextView>
          Jeśli brak krążenia i oddechu rozpocząć resuscytację. Postępowanie zależne od temperatury
          głębokiej pacjenta.
        </MyTextView>
        <AskTextView>Temperatura poniżej 30 stopni C?</AskTextView>
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(<Tak navigation={navigation} />)}
          onPressNo={() => this.handleOnClick(<Nie navigation={navigation} />)}
        />
        <MyTextView />
        {divy}
      </ScrollContainer>
    );
  }
}
