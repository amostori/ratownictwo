import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollContainer } from '../../../components/container';
import { MyTextView, Hypertekst, VersionTextView } from '../../../components/TextViews';

export default class NoNitro extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleNitro = () => {
    const { navigation } = this.props;
    navigation.navigate('Nitrogliceryna');
  };

  handleMorfina = () => {
    const { navigation } = this.props;
    navigation.navigate('Morfina');
  };

  handleHeparyna = () => {
    const { navigation } = this.props;
    navigation.navigate('Heparyna');
  };

  handleBrylic = () => {
    const { navigation } = this.props;
    navigation.navigate('Tikagrelor');
  };

  handleKlopidogrel = () => {
    const { navigation } = this.props;
    navigation.navigate('Klopidogrel');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          Nie podawaj
          <Hypertekst onPress={this.handleNitro}>Nitrogliceryny</Hypertekst>
        </MyTextView>
        <MyTextView>
          Jeśli ból wymaga zniesienia podaj
          <Hypertekst onPress={this.handleMorfina}>Morfinę</Hypertekst>
4 - 8 mg iv. Kolejne dawki po
          2 mg co 15 min do zmniejszenia dolegliwości.
        </MyTextView>
        <MyTextView>
          Tylko za zgodą lekarza oceniającego EKG podaj
          <Hypertekst onPress={this.handleHeparyna}>Heparynę</Hypertekst>
          5000 UI iv oraz
          <Hypertekst onPress={this.handleBrylic}>Tikagrelor</Hypertekst>
          180 mg p.o.
        </MyTextView>
        <MyTextView>
          W przypadku chorych przyjmujących acenokumarol lub inny doustny lek przeciwkrzepliwy oraz
          w przypadku chorych po przebytym krwawieniu śródczaszkowym zamiast Tikagreloru podaj
          <Hypertekst onPress={this.handleKlopidogrel}>Klopidogrel</Hypertekst>
          600 mg p.o.
        </MyTextView>
        <VersionTextView>
          Na podstawie wytycznych Europejskiego Towarzystwa Kardiologicznego, podręcznika
          &quot;Ostre Stany Wieńcowe&quot; Adama Stępki wyd. 2015 oraz wytycznych ERC 2015.
        </VersionTextView>
      </ScrollContainer>
    );
  }
}
