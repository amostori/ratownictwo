import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollContainer } from '../../components/container';
import {
  MyTextView,
  Hypertekst,
  BoldText,
  TiM,
  VersionSzczeklikERC,
} from '../../components/TextViews';

export default class BolBrzucha extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleMorfina = () => {
    const { navigation } = this.props;
    navigation.navigate('Morfina');
  };

  handleParacetamol = () => {
    const { navigation } = this.props;
    navigation.navigate('Paracetamol');
  };

  handlePyralgina = () => {
    const { navigation } = this.props;
    navigation.navigate('Pyralgina');
  };

  handlePapaweryna = () => {
    const { navigation } = this.props;
    navigation.navigate('Papaweryna');
  };

  handleDrotaweryna = () => {
    const { navigation } = this.props;
    navigation.navigate('Drotaweryna');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <BoldText>Pamiętaj</BoldText>
          by wykonać EKG. Ból brzucha może być objawem m.in. zawału serca.
        </MyTextView>
        <MyTextView>
          Jeśli ból wymaga zniesienia (zwykle &gt;7 punktów w skali bólu) podaj
          <Hypertekst onPress={this.handleParacetamol}>Paracetamol</Hypertekst>
1 g we wlewie iv (15
          min).
        </MyTextView>
        <MyTextView>
          Jeśli ból nie ustąpił podaj
          <Hypertekst onPress={this.handleMorfina}>Morfinę</Hypertekst>
4 - 8 mg iv. Kolejne dawki po
          2 mg co 15 min do ustąpienia bólu.
        </MyTextView>
        <MyTextView>
          W przypadku kolki wątrobowej nie podawaj Morfiny, podaj
          <Hypertekst onPress={this.handlePyralgina}>Metamizol</Hypertekst>
          (Pyralgina) 2,5 g w 100 ml 0,9% NaCl iv w ciągu 5 min.
        </MyTextView>
        <MyTextView>
          W przypadku bólu o charakterze kolki rozważ dodatkowo leki rozkurczowe:
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleDrotaweryna}>Drotaweryna</Hypertekst>
          (No - Spa) 40 mg w 20 ml 0,9% NaCl powoli iv
        </MyTextView>
        <MyTextView>lub</MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handlePapaweryna}>Papaweryna</Hypertekst>
          40 mg i.m.
        </MyTextView>
        <TiM />
        <VersionSzczeklikERC />
      </ScrollContainer>
    );
  }
}
