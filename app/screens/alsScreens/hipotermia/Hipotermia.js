import React from 'react';
import PropTypes from 'prop-types';
import { ButtonsYesNo } from '../../../components/Buttons';
import { MyTextView, AskTextView } from '../../../components/TextViews';
import { ScrollContainer } from '../../../components/container';
import Nie from './Nie';
import Tak from './Tak';

class Hipotermia extends React.Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  render() {
    const { navigation } = this.props;
    const { divy } = this.state;

    return (
      <ScrollContainer>
        <MyTextView>Ocena oddechu i krążenia wydłużona do 1 minuty.</MyTextView>
        <AskTextView>Zaburzenia świadomości?</AskTextView>
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(<Tak navigation={navigation} />)}
          onPressNo={() => this.handleOnClick(<Nie navigation={navigation} />)}
        />
        <MyTextView />
        {divy}
      </ScrollContainer>
    );
  }
}

export default Hipotermia;
