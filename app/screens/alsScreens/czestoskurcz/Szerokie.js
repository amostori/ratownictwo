import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { ButtonsYesNo } from '../../../components/Buttons';
import { AskTextView, MyTextView, BoldText } from '../../../components/TextViews';
import SzerokieMiarowy from './SzerokieMiarowy';
import SzerokieNieMiarowy from './SzerokieNieMiarowy';

export default class Szerokie extends Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  render() {
    const { divy } = this.state;
    const { navigation } = this.props;

    return (
      <View>
        <MyTextView>
          <BoldText>Częstoskurcz z szerokimi QRS</BoldText>
        </MyTextView>

        <AskTextView>Rytm jest miarowy?</AskTextView>
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(<SzerokieMiarowy navigation={navigation} />)}
          onPressNo={() => this.handleOnClick(<SzerokieNieMiarowy navigation={navigation} />)}
        />
        <MyTextView />
        {divy}
      </View>
    );
  }
}
