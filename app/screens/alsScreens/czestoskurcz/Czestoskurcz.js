import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { ScrollContainer } from '../../../components/container';
import {
  AskTextView,
  MyTextView,
  ListaTextView,
  BoldText,
  Hypertekst,
  VersionERC,
} from '../../../components/TextViews';
import { ButtonsYesNo } from '../../../components/Buttons';
import Stabilny from './Stabilny';

export default class Czestoskurcz extends Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  handleSedacja = () => {
    const { navigation } = this.props;
    navigation.navigate('Sedacja');
  };

  handleKardiowersja = () => {
    const { navigation } = this.props;
    navigation.navigate('Kardiowersja');
  };

  handleAmiodaron = () => {
    const { navigation } = this.props;
    navigation.navigate('Amiodaron');
  };

  handleMagnez = () => {
    const { navigation } = this.props;
    navigation.navigate('Magnez');
  };

  handleLignokaina = () => {
    const { navigation } = this.props;
    navigation.navigate('Lignokaina');
  };

  render() {
    const { navigation } = this.props;
    const { divy } = this.state;
    const Niestabilny = (
      <View>
        <BoldText>NIESTABILNY</BoldText>
        <MyTextView />

        <MyTextView>
          Jeśli pacjent jest przytomny zastosuj
          <Hypertekst onPress={this.handleSedacja}>sedację i analgezję.</Hypertekst>
        </MyTextView>
        <MyTextView />
        <MyTextView>
          <Hypertekst onPress={this.handleKardiowersja}>Kardiowersja elektryczna</Hypertekst>
        </MyTextView>

        <MyTextView />
        <MyTextView>
          Jeśli częstoskurcz nadal się utrzymuje wykonaj kolejną próbę kardiowersji (łącznie
          maksymalnie 3 razy).
        </MyTextView>
        <MyTextView>
          Jeśli brak poprawy wlew
          <Hypertekst onPress={this.handleAmiodaron}>amiodaronu</Hypertekst>
          300 mg w 250 ml 5% glukozy w przeciągu 10 - 20 min.
        </MyTextView>
        <MyTextView>
          W przypadku Torsade de Pointes Amiodaron jest przeciwwskazany. Podaj
          <Hypertekst onPress={this.handleMagnez}>Magnez</Hypertekst>
2 g (10 ml 20% roztworu) iv
          przez 10 min i rozważ
          <Hypertekst onPress={this.handleLignokaina}>Lignokainę</Hypertekst>
          50 - 100 mg iv (5 - 10 ml 1% roztworu).
        </MyTextView>
        <MyTextView>
          Amiodaron jest również przeciwwskazany w zatruciu trójcyklicznymi lekami
          przeciwdepresyjnymi. W takim przypadku również zastosuj Lignokainę.
        </MyTextView>
        <MyTextView>Monitoring i transport na SOR.</MyTextView>
        <VersionERC />
      </View>
    );
    return (
      <ScrollContainer>
        <AskTextView>Występują objawy niepokojące?</AskTextView>
        <ListaTextView>1. Wstrząs?</ListaTextView>
        <ListaTextView>2. Omdlenie?</ListaTextView>
        <ListaTextView>3. Niedokrwienie mięśnia sercowego?</ListaTextView>
        <ListaTextView>4. Niewydolność serca?</ListaTextView>
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(Niestabilny)}
          onPressNo={() => this.handleOnClick(<Stabilny navigation={navigation} />)}
        />
        <MyTextView />
        {divy}
      </ScrollContainer>
    );
  }
}
