import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollContainer } from '../../components/container';
import { MyTextView, Hypertekst, VersionERC } from '../../components/TextViews';

export default class Astma extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleHydrokortyzon = () => {
    const { navigation } = this.props;
    navigation.navigate('Hydrokortyzon');
  };

  handleDexaven = () => {
    const { navigation } = this.props;
    navigation.navigate('Dexaven');
  };

  handleSalbutamol = () => {
    const { navigation } = this.props;
    navigation.navigate('Salbutamol');
  };

  handleMagnez = () => {
    const { navigation } = this.props;
    navigation.navigate('Magnez');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>Tlen, aby utrzymać saturację &gt;94%</MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleSalbutamol}>Salbutamol</Hypertekst>
5 mg w nebulizacji. W
          razie potrzeby powtarzać co 15 min.
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleHydrokortyzon}>Hydrokortyzon</Hypertekst>
          200 mg iv.
        </MyTextView>
        <MyTextView>
          W przypadku astmy aspirynowej zamiast hydrokortyzonu podaj
          <Hypertekst onPress={this.handleDexaven}>Deksametazon</Hypertekst>
8 mg w 20 ml 0,9% NaCl
          iv.
        </MyTextView>
        <MyTextView>
          Jeśli brak poprawy rozpocznij transport do szpitala i podaj
          <Hypertekst onPress={this.handleMagnez}>Magnez</Hypertekst>
1 - 2 g we wlewie przez 20 min
          (w POCHP niezalecane).
        </MyTextView>
        <MyTextView>
          Jeśli przyczyną zaostrzenia astmy może być reakcja anafilaktyczna postępuj jak w protokole
          Anafilaksji.
        </MyTextView>
        <MyTextView>W przypadku zatrzymania krążenia wentyluj z częstością 8 - 10/min.</MyTextView>
        <MyTextView>
          Rozważ chwilowe rozłączenie rurki intubacyjnej w celu uwolnienia uwięzionego powietrza
          jeśli podejrzewasz nadmierne rozdęcie płuc (pułapka powietrzna).
        </MyTextView>
        <VersionERC />
      </ScrollContainer>
    );
  }
}
