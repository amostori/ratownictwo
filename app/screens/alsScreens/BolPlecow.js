import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollContainer } from '../../components/container';
import {
  MyTextView, Hypertekst, TiM, VersionERC,
} from '../../components/TextViews';

export default class BolPlecow extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleMorfina = () => {
    const { navigation } = this.props;
    navigation.navigate('Morfina');
  };

  handleParacetamol = () => {
    const { navigation } = this.props;
    navigation.navigate('Paracetamol');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          Oceń nasilenie bólu w skali 0 - 10 punktów. Jeśli ból jest poniżej 7 podaj
          <Hypertekst onPress={this.handleParacetamol}>Paracetamol</Hypertekst>
1 g w 15 minutowym
          wlewie i.v.
        </MyTextView>
        <MyTextView>
          Jeśli ból jest silniejszy niż 6 punktów podaj
          <Hypertekst onPress={this.handleMorfina}>Morfinę</Hypertekst>
4 - 8 mg powoli i.v. Kolejne
          dawki po 2 mg co 15 min do ustąpienia bólu.
        </MyTextView>
        <MyTextView>
          Uwaga, pacjent wymaga transportu do szpitala w celu wykluczenia stanów zagrożenia życia
          (np. tętniaka rozwarstwiającego aorty).
        </MyTextView>
        <MyTextView>
          Najczęstszą przyczyną tzw. &quot;Rwy kulszowej&quot; jest dyskopatia.
        </MyTextView>
        <MyTextView>
          Charakteryzuje się nagłym bólem okolicy lędźwiowej, promieniującym do pośladka, tylnej
          powierzchni uda, do łydki albo stopy.
        </MyTextView>
        <MyTextView>
          Charakterystyczny jest objaw Lasegue (czyt. Laseka), czyli wystąpienie bólu wzdłuż
          wyprostowanej kończyny dolnej podczas jej biernego unoszenia.
        </MyTextView>
        <TiM />
        <VersionERC />
      </ScrollContainer>
    );
  }
}
