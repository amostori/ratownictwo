import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollContainer } from '../../components/container';
import {
  MyTextView, Hypertekst, BoldText, VersionERC, TiM,
} from '../../components/TextViews';

export default class Anafilaksja extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleAdrenalina = () => {
    const { navigation } = this.props;
    // destrukturyzacja props przesłanych z NzkAlsScreen. Jednym z elementów musi być "navigation"
    navigation.navigate('Adrenalina');
  };

  handleHydrokortyzon = () => {
    const { navigation } = this.props;
    // destrukturyzacja props przesłanych z NzkAlsScreen. Jednym z elementów musi być "navigation"
    navigation.navigate('Hydrokortyzon');
  };

  handleDexaven = () => {
    const { navigation } = this.props;
    // destrukturyzacja props przesłanych z NzkAlsScreen. Jednym z elementów musi być "navigation"
    navigation.navigate('Dexaven');
  };

  handleClemastin = () => {
    const { navigation } = this.props;
    // destrukturyzacja props przesłanych z NzkAlsScreen. Jednym z elementów musi być "navigation"
    navigation.navigate('Clemastin');
  };

  handleGlukagon = () => {
    const { navigation } = this.props;
    // destrukturyzacja props przesłanych z NzkAlsScreen. Jednym z elementów musi być "navigation"
    navigation.navigate('Glukagon');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <Hypertekst onPress={this.handleAdrenalina}>Adrenalina 0,5 mg i.m.</Hypertekst>
          (w udo).
        </MyTextView>
        <MyTextView>
          500 - 1000 ml 0,9%
          <BoldText>NaCl</BoldText>
          iv.
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleHydrokortyzon}>Hydrokortyzon</Hypertekst>
          200 mg iv.
        </MyTextView>
        <MyTextView>
          <MyTextView>
            W przypadku astmy aspirynowej zamiast hydrokortyzonu podaj
            <Hypertekst onPress={this.handleDexaven}>Deksametazon</Hypertekst>
8 mg powoli i.v. w
            rozcieńczeniu do 20 ml NaCl 0,9%.
          </MyTextView>
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleClemastin}>Clemastin</Hypertekst>
2 mg w 10 ml 0,9% NaCl
          iv.
        </MyTextView>
        <MyTextView>
          Jeśli pacjent jest leczony betablokerami zalecany jest
          <Hypertekst onPress={this.handleGlukagon}>Glukagon</Hypertekst>
1 mg i.v., ale w Polsce
          brak preparatu dożylnego.
        </MyTextView>
        <MyTextView>
          Jeśli brak efektu po 5 minutach powtórz
          <BoldText>Adrenalinę.</BoldText>
        </MyTextView>
        <TiM />
        <VersionERC />
      </ScrollContainer>
    );
  }
}
