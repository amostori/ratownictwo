import React from 'react';
import { ScrollView, View } from 'react-native';
import { styles } from '../../components/Images/HomeImageButton';
import { MyTextView } from '../../components/TextViews';

const Amiodaron = () => (
  <ScrollView>
    <View style={styles.styles}>
      <MyTextView>Amiodaron</MyTextView>
    </View>
  </ScrollView>
);

export default Amiodaron;
