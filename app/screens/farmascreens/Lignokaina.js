import React from 'react';
import { ScrollView, View } from 'react-native';
import { styles } from '../../components/Images/HomeImageButton';
import { MyTextView } from '../../components/TextViews';

const Lignokaina = () => (
  <ScrollView>
    <View style={styles.styles}>
      <MyTextView>Lignokaina</MyTextView>
    </View>
  </ScrollView>
);

export default Lignokaina;
