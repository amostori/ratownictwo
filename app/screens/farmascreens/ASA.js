import React from 'react';
import { ScrollView, View } from 'react-native';
import { styles } from '../../components/Images/HomeImageButton';
import { MyTextView } from '../../components/TextViews';

const ASA = () => (
  <ScrollView>
    <View style={styles.styles}>
      <MyTextView>ASA</MyTextView>
    </View>
  </ScrollView>
);

export default ASA;
