import React from 'react';
import { ScrollView, View } from 'react-native';
import { styles } from '../../components/Images/HomeImageButton';
import { MyTextView } from '../../components/TextViews';

const Budezonid = () => (
  <ScrollView>
    <View style={styles.styles}>
      <MyTextView>Budezonid</MyTextView>
    </View>
  </ScrollView>
);

export default Budezonid;
