import React from 'react';
import { ScrollContainer } from '../../components/container';
import {
  MyTextView, BoldText, ListaTextView, VersionERC,
} from '../../components/TextViews';

const Porod = () => (
  <ScrollContainer>
    <MyTextView>
      <BoldText>Szybka ocena i wywiad położniczy:</BoldText>
    </MyTextView>
    <ListaTextView>1. Który to jest poród?</ListaTextView>
    <ListaTextView>2. Jak długo trwał ostatni poród?</ListaTextView>
    <ListaTextView>3. Czy pacjentka odczuwa potrzebę parcia?</ListaTextView>
    <ListaTextView>4. Czy główka jest widoczna lub pojawia się w czasie skurczu?</ListaTextView>
    <MyTextView />
    <MyTextView>
      Jeśli pacjentka jest wieloródką, ostatni poród trwał krótko, pacjentka prze w czasie skurczu
      lub widoczna jest główka nie ryzykuj transportu do szpitala. Przygotuj się do porodu na
      miejscu
    </MyTextView>
    <MyTextView>
      <BoldText>Ocena ryzyka powikłań:</BoldText>
    </MyTextView>
    <ListaTextView>1. Termin porodu? (&lt;36 tygodnia: wcześniak).</ListaTextView>
    <ListaTextView>2. Czy to jest ciąża mnoga? (może być potrzebna druga karetka).</ListaTextView>
    <ListaTextView>
      3. Kolor wód płodowych? (zanieczyszczenie smółką - ryzyko niedrożności dróg oddechowych).
    </ListaTextView>
    <MyTextView />
    <MyTextView>
      <BoldText>Procedura odebrania porodu:</BoldText>
    </MyTextView>
    <MyTextView>
      1. Rozpakuj zestaw porodowy. Ułóż jedną chustę pod pośladkami a drugą na brzuchu rodzącej.
      Załóż rękawiczki jałowe.
    </MyTextView>
    <MyTextView>
      2. Asekuruj jedną ręką, by główka nie wyskoczyła zbyt szybko, a drugą zabezpieczaj krocze
      przed rozerwaniem.
    </MyTextView>
    <MyTextView>
      3. Kiedy główka wyjdzie dziecko zacznie się rotować, aby mogły urodzić się barki. Oceń czy
      wokół szyi nie ma pępowiny. W razie czego zsuń ją z główki.
    </MyTextView>
    <MyTextView>
      4. Kiedy pojawią się ramiona najpierw delikatnie skieruj główkę w dół aby urodzić górny bark,
      następnie w górę, by uwolnić dolny bark.
    </MyTextView>
    <MyTextView>5. Osusz, okryj, oceń dziecko.</MyTextView>
    <ListaTextView>
      Odessać smółkę tylko jeśli jest widoczna w drogach oddechowych i świeżorodek jest mało
      żywotny.
    </ListaTextView>
    <MyTextView />
    <MyTextView>
      6. Po około minucie załóż zacisk ok. 10 cm od brzuszka dziecka, następnie 10 cm dalej i
      odetnij pępowinę między zaciskami. Zbezpiecz końcówki pępowiny jałowymi gazikami.
    </MyTextView>
    <MyTextView>7. Podaj dziecko matce tak, by mogło ssać pierś i okryj oboje.</MyTextView>
    <VersionERC />
  </ScrollContainer>
);

export default Porod;
