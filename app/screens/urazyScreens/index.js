import ZdarzenieMasowe from './ZdarzenieMasowe';
import TriageAdult from './triageAdult/TriageAdult';
import TriageJump from './triageJump/TriageJump';
import CentrumUrazowe from './CentrumUrazowe';
import Czaszka from './Czaszka';

export {
  ZdarzenieMasowe, TriageAdult, TriageJump, CentrumUrazowe, Czaszka,
};
