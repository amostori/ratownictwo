import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { ButtonsYesNo } from '../../components/Buttons';
import { ScrollContainer } from '../../components/container';
import {
  Sol,
  ListaTextView,
  VersionTextView,
  Hypertekst,
  MyTextView,
  AskTextView,
} from '../../components/TextViews';

export default class Czaszka extends React.Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  handleHiperwentylacja = () => {
    const { navigation } = this.props;
    navigation.navigate('Hiperwentylacja');
  };

  render() {
    const { divy } = this.state;

    const tak = (
      <View>
        <MyTextView>
          Rozważ wezwanie karetki S lub LPR w celu zaintubowania pacjenta i prowadzenia
          <Hypertekst onPress={this.handleHiperwentylacja}>hiperwentylacji.</Hypertekst>
        </MyTextView>
        <MyTextView>
          Transportuj w pozycji z uniesionym tułowiem pod kątem 30 stopni, unikając gwałtownych
          zahamowań i wstrząsów.
        </MyTextView>
        <VersionTextView>
          Na podstawie wytycznych Brain Trauma Foundation i International Trauma Life Support.
        </VersionTextView>
      </View>
    );
    const nie = (
      <View>
        <MyTextView>
          Transportuj w pozycji z uniesionym tułowiem pod kątem 30 stopni, unikając gwałtownych
          zahamowań i wstrząsów.
        </MyTextView>
        <VersionTextView>
          Na podstawie wytycznych Brain Trauma Foundation i International Trauma Life Support.
        </VersionTextView>
      </View>
    );
    return (
      <ScrollContainer>
        <MyTextView>
          Zapewnij drożność dróg oddechowych i podaj tlen, tak, by utrzymać saturację conajmniej
          90%.
        </MyTextView>
        <MyTextView>
          Podaj wlew
          <Sol />
          by utrzymać ciśnienie skurczowe &gt;90 mmHg (&gt;110 wg ITLS). U dzieci utrzymuj
          fizjologiczne wartości ciśnienia w zależności od wieku.
        </MyTextView>
        <AskTextView>Objawy wgłobienia?</AskTextView>
        <ListaTextView>
          Ocena GCS &lt;9 i pozycja odmóżdżeniowa (patologiczny wyprost)?
        </ListaTextView>
        <ListaTextView>lub</ListaTextView>
        <ListaTextView>
          Ocena GCS &lt;9 i niesymetryczne (lub obustronne) poszerzenie źrenic?
        </ListaTextView>
        <ListaTextView>lub</ListaTextView>
        <ListaTextView>
          Początkowa ocena GCS &lt;9, która szybko obniża się o ponad 2 punkty?
        </ListaTextView>
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(tak)}
          onPressNo={() => this.handleOnClick(nie)}
        />
        <MyTextView />
        {divy}
      </ScrollContainer>
    );
  }
}
