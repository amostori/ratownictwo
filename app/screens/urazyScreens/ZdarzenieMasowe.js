import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollContainer } from '../../components/container';
import { MyTextView, BadanieHipertekst } from '../../components/TextViews';
import { TriageColorButtons } from '../../components/Buttons';

export default class ZdarzenieMasowe extends Component {
  state = {
    green: 0,
    yellow: 0,
    red: 0,
    black: 0,
    total: 0,
  };

  handleTotalIncrease = (tot) => {
    this.setState({
      total: tot + 1,
    });
  };

  handleIncreaseBlack = (bl, tot) => {
    this.setState({
      black: bl + 1,
      total: tot + 1,
    });
  };

  handleIncreaseGreen = (gr, tot) => {
    this.setState({
      green: gr + 1,
      total: tot + 1,
    });
  };

  handleIncreaseRed = (re, tot) => {
    this.setState({
      red: re + 1,
      total: tot + 1,
    });
  };

  handleIncreaseYellow = (ye, tot) => {
    this.setState({
      yellow: ye + 1,
      total: tot + 1,
    });
  };

  handleTotalReset = () => {
    this.setState({
      total: 0,
      black: 0,
      red: 0,
      yellow: 0,
      green: 0,
    });
  };

  render() {
    const {
      total, black, green, yellow, red,
    } = this.state;
    return (
      <ScrollContainer>
        <BadanieHipertekst onPress={this.handleTotalReset}>
          Razem:
          {total}
        </BadanieHipertekst>

        <TriageColorButtons
          onPressGreen={() => this.handleIncreaseGreen(green, total)}
          onPressYellow={() => this.handleIncreaseYellow(yellow, total)}
          onPressRed={() => this.handleIncreaseRed(red, total)}
          onPressBlack={() => this.handleIncreaseBlack(black, total)}
          greenTekst={green}
          yellowTekst={yellow}
          redTekst={red}
          blackTekst={black}
        />
      </ScrollContainer>
    );
  }
}
