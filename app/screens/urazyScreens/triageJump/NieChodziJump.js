import React from 'react';
import { View } from 'react-native';
import { ButtonsYesNo } from '../../../components/Buttons';
import { MyTextView, AskTextView } from '../../../components/TextViews';
import OddychaWoutAJump from './OddychaWoutAJump';
import NieOddychaWoutAJump from './NieOddychaWoutAJump';

export default class NieChodziJump extends React.Component {
  state = {
    divy: null,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  render() {
    const { divy } = this.state;

    return (
      <View>
        <AskTextView>Oddycha bez udrożnienia dróg oddechowych?</AskTextView>

        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(<OddychaWoutAJump />)}
          onPressNo={() => this.handleOnClick(<NieOddychaWoutAJump />)}
        />
        <MyTextView />
        {divy}
      </View>
    );
  }
}
