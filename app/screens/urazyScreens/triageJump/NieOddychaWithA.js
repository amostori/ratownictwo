import React from 'react';
import { View } from 'react-native';
import { ButtonsYesNo, ButtonTriageAdult } from '../../../components/Buttons';
import { MyTextView, AskTextView } from '../../../components/TextViews';
import TetnoObecneJump from './TetnoObecneJump';

export default class NieOddychaWithA extends React.Component {
  state = {
    divy: null,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  render() {
    const { divy } = this.state;
    const Czarny = (
      <ButtonTriageAdult kolor="black" tekstKolor="white">
        CZARNY
      </ButtonTriageAdult>
    );
    return (
      <View>
        <AskTextView>Tętno obecne na tętnicy promieniowej?</AskTextView>

        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(<TetnoObecneJump />)}
          onPressNo={() => this.handleOnClick(Czarny)}
        />
        <MyTextView />
        {divy}
        <MyTextView />
      </View>
    );
  }
}
