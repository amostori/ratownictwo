import React from 'react';
import { View } from 'react-native';
import { ButtonsYesNo } from '../../../components/Buttons';
import { MyTextView, AskTextView } from '../../../components/TextViews';
import OddychaWoutA from './OddychaWoutA';
import NieOddychaWoutA from './NieOddychaWoutA';

export default class NieChodzi extends React.Component {
  state = {
    divy: null,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  render() {
    const { divy } = this.state;

    return (
      <View>
        <AskTextView>Oddycha bez udrożnienia dróg oddechowych?</AskTextView>

        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(<OddychaWoutA />)}
          onPressNo={() => this.handleOnClick(<NieOddychaWoutA />)}
        />
        <MyTextView />
        {divy}
      </View>
    );
  }
}
