import React from 'react';
import { ButtonsYesNo, ButtonTriageAdult } from '../../../components/Buttons';
import { MyTextView, AskTextView } from '../../../components/TextViews';
import { ScrollContainer } from '../../../components/container';
import NieChodzi from './NieChodzi';

export default class TriageAdult extends React.Component {
  state = {
    divy: null,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  render() {
    const { divy } = this.state;

    const Zielony = (
      <ButtonTriageAdult kolor="green" tekstKolor="white">
        ZIELONY
      </ButtonTriageAdult>
    );

    return (
      <ScrollContainer>
        <AskTextView>Może chodzić?</AskTextView>

        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(Zielony)}
          onPressNo={() => this.handleOnClick(<NieChodzi />)}
        />
        <MyTextView />
        {divy}
      </ScrollContainer>
    );
  }
}
