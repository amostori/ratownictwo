import React from 'react';
import { View } from 'react-native';
import { ButtonsYesNo, ButtonTriageAdult } from '../../../components/Buttons';
import { MyTextView, AskTextView } from '../../../components/TextViews';

export default class TetnoObecne extends React.Component {
  state = {
    divy: null,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  render() {
    const { divy } = this.state;
    const Czerwony = (
      <ButtonTriageAdult kolor="red" tekstKolor="white">
        CZERWONY
      </ButtonTriageAdult>
    );
    const Zolty = (
      <ButtonTriageAdult kolor="yellow" tekstKolor="black">
        ŻÓŁTY
      </ButtonTriageAdult>
    );

    return (
      <View>
        <AskTextView>Spełnia polecenia?</AskTextView>

        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(Zolty)}
          onPressNo={() => this.handleOnClick(Czerwony)}
        />
        <MyTextView />
        {divy}
        <MyTextView />
      </View>
    );
  }
}
