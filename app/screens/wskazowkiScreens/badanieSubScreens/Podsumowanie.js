import React from 'react';
import { Image } from 'react-native';
import { MyTextView, BoldText } from '../../../components/TextViews';
import { WskazowkiContainer } from '../../../components/container';
import styles from '../styles';

const Podsumowanie = () => (
  <WskazowkiContainer>
    <Image source={require('../images/stetoskop.png')} style={styles.image} resizeMode="contain" />
    <MyTextView>
      Na koniec zbieramy wszystkie zlecone wcześniej wyniki badań tj. saturację, cukier, ciśnienie i
      temperaturę. Interpretujemy EKG.
    </MyTextView>
    <MyTextView>
      Wiemy już wszystko o pacjencie. Stawiamy diagnozę ratowniczą i podejmujemy decyzje
      terapeutyczne. Następnie przewozimy chorego do szpitala, gdzie przekazujemy na SOR zgodnie ze
      schematem
      <BoldText>AT - MIST</BoldText>
    </MyTextView>
    <MyTextView>
      <BoldText>A (age) -</BoldText>
      wiek, płeć i istotny wywiad (np. ciąża, zażywanie acenokumarolu).
    </MyTextView>
    <MyTextView>
      <BoldText>T (time of trauma) - </BoldText>
      czas wystąpienia urazu, zachorowania.
    </MyTextView>
    <MyTextView>
      <BoldText>M (mechanism) - </BoldText>
      mechanizm urazu, zachorowania.
    </MyTextView>
    <MyTextView>
      <BoldText>I (injury) - </BoldText>
      podejrzewane obrażenia, choroby.
    </MyTextView>
    <MyTextView>
      <BoldText>S (symptoms) - </BoldText>
      objawy.
    </MyTextView>
    <MyTextView>
      <BoldText>T (treatment) - </BoldText>
      podjęte interwencje i leczenie.
    </MyTextView>
  </WskazowkiContainer>
);

export default Podsumowanie;
