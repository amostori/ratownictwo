import Wstep from './Wstep';
import FunkcjeZyciowe from './FunkcjeZyciowe';
import Zlecenia from './Zlecenia';
import BadanieKliniczne from './BadanieKliniczne';
import Podsumowanie from './Podsumowanie';

export {
  Wstep, FunkcjeZyciowe, Zlecenia, BadanieKliniczne, Podsumowanie,
};
