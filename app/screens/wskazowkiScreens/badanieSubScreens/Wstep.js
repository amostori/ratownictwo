import React from 'react';
import { Image } from 'react-native';
import { MyTextView, BoldText } from '../../../components/TextViews';
import { WskazowkiContainer } from '../../../components/container';
import styles from '../styles';

const Wstep = () => (
  <WskazowkiContainer>
    <Image source={require('../images/stetoskop.png')} style={styles.image} resizeMode="contain" />
    <MyTextView>
      Pierwszy etap to trzy kroki, które można skrótowo zapamiętać jako
      <BoldText>BLS:</BoldText>
      bezpieczeństwo, liczba poszkodowanych oraz siły i środki dodatkowe.
    </MyTextView>
    <MyTextView>
      Kierownik zespołu sprawdza czy jest bezpiecznie, prosi drugą osobę z zespołu o ocenę liczby
      poszkodowanych i kontaktuje się z dyspozytorem jeśli potrzebna jest dodatkowa pomoc.
    </MyTextView>
  </WskazowkiContainer>
);

export default Wstep;
