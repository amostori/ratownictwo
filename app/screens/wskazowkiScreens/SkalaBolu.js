import React from 'react';
import { Image } from 'react-native';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

const SkalaBolu = () => (
  <WskazowkiContainer>
    <Image
      style={styles.skalaBolu}
      source={require('./images/twarzyczki.jpg')}
      resizeMode="contain"
    />
  </WskazowkiContainer>
);

export default SkalaBolu;
