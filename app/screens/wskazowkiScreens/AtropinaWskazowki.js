import React from 'react';
import { Image } from 'react-native';
import { MyTextView, Sol, BoldText } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

const AtropinaWskazowki = () => (
  <WskazowkiContainer>
    <Image source={require('./images/kid.png')} style={styles.image} resizeMode="contain" />
    <MyTextView>Dawka atropiny u dzieci to 0,02 mg/kg, ale minimum 0,1 mg.</MyTextView>
    <MyTextView>
      <BoldText>Dziecko &lt;3 m.ż.</BoldText>
    </MyTextView>
    <MyTextView>
      Nabierz 1 mg atropiny do strzykawki dziesiątki i rozcieńcz do 10 ml
      <Sol />
. Podaj 1 ml iv lub i.o. Dawka 0,1 mg leku.
    </MyTextView>
    <MyTextView>
      <BoldText>Dziecko &gt;2 m.ż.</BoldText>
    </MyTextView>
    <MyTextView>
      Nabierz 2 mg atropiny do strzykawki dziesiątki i rozcieńcz do 10 ml
      <Sol />
. Podaj 0,1 ml/kg czyli w 1 ml jest dawka na 10 kg wagi ciała.
    </MyTextView>
  </WskazowkiContainer>
);

export default AtropinaWskazowki;
