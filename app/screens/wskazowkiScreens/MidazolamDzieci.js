import React from 'react';
import { Image } from 'react-native';
import { MyTextView, BoldText, Sol } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

const MidazolamDzieci = () => (
  <WskazowkiContainer>
    <Image source={require('./images/kid.png')} style={styles.image} resizeMode="contain" />
    <MyTextView>
      W przypadku drgawek dawka midazolamu to
      <BoldText>0,1 mg/kg:</BoldText>
    </MyTextView>
    <MyTextView>
      Nabierz ampułkę (5 mg) do strzykawki &quot;dziesiątki&quot;, ale
      <BoldText>UWAGA,</BoldText>
      rozcieńcz do 5 ml. Teraz podawaj w dawce 0,1 ml/kg, czyli w 1 ml jest dawka na 10 kg wagi
      ciała.
    </MyTextView>
    <MyTextView>
      W analgosedacji dawka początkowa to
      <BoldText>0,05 mg/kg.</BoldText>
    </MyTextView>
    <MyTextView>
      Rozcieńcz 5 mg w 10 ml
      <Sol />
i podawaj po 0,1 ml/kg, czyli w 1 ml jest dawka na 10 kg wagi ciała.
    </MyTextView>
  </WskazowkiContainer>
);

export default MidazolamDzieci;
