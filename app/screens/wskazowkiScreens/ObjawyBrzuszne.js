import React from 'react';
import { Image } from 'react-native';
import { MyTextView, BoldText } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

const ObjawyBrzuszne = () => (
  <WskazowkiContainer>
    <Image source={require('./images/stetoskop.png')} style={styles.image} resizeMode="contain" />
    <MyTextView>
      <BoldText>Objaw Blumberga</BoldText>
    </MyTextView>
    <MyTextView>
      Nagły, ostry ból po szybkim cofnięciu ręki. Dodatni w zapaleniu otrzewnej.
    </MyTextView>
    <MyTextView>
      <BoldText>Objaw Rovsinga</BoldText>
    </MyTextView>
    <MyTextView>
      Badamy przykładając jedną rękę w lewym dolnym kwadrancie brzucha, prostopadle do okrężnicy a
      drugą przesuwając w stronę prawego dolnego kwadrantu. Objaw uznaje się za dodatni kiedy
      reakcją na tę czynność jest pojawienie się bólu odczuwanego w prawym dole biodrowym (efekt
      wzrostu ciśnienia gazów w tym miejscu). Typowo pojawia się w zapaleniu wyrostka robaczkowego
      lub zapaleniu otrzewnej.
    </MyTextView>
    <MyTextView>
      <BoldText>Objaw Jaworskiego</BoldText>
    </MyTextView>
    <MyTextView>
      Początkowo unosimy prawą kończynę dolną, a następnie drugą ręką uciskamy brzuch w okolicy
      wyrostka robaczkowego. Jeśli podczas opuszczania nogi pojawi się ból objaw uważa się za
      dodatni (zapalenie wyrostka robaczkowego).
    </MyTextView>
    <MyTextView>
      <BoldText>Objaw Murphy&apos;ego</BoldText>
    </MyTextView>
    <MyTextView>
      Bada się przez położenie dłoni w okolicy podżebrowej prawej w linii środkowo-obojczykowej i
      nakazanie choremu wykonanie głębokiego wdechu. Objaw jest dodatni jeśli ból spowoduje
      przerwanie wdechu. Dodatni w kamicy pęcherzyka żółciowego, ujemny w kamicy przewodu żółciowego
      i zapaleniu dróg żółciowych.
    </MyTextView>
    <MyTextView>
      <BoldText>Objaw Chełmońskiego</BoldText>
    </MyTextView>
    <MyTextView>
      Badanie tego objawu polega na położeniu dłoni na łuku żebrowym prawym i następnie uderzeniu w
      dłoń pięścią drugiej ręki co powoduje silny ból w zapaleniu pęcherzyka żółciowego i kamicy
      żółciowej.
    </MyTextView>
    <MyTextView>
      <BoldText>Objaw dzwonu śmierci</BoldText>
    </MyTextView>
    <MyTextView>
      Brak perystaltyki powoduje, że osłuchując brzuch słychać tylko tętno aorty brzusznej. Pojawia
      się w niedrożności jelit.
    </MyTextView>
    <MyTextView>
      <BoldText>Objaw Cullena</BoldText>
    </MyTextView>
    <MyTextView>
      Brunatnosine przebarwienia w okolicy pępka u chorych z ciężką postacią ostrego zapalenia
      trzustki.
    </MyTextView>
    <MyTextView>
      <BoldText>Objaw Grey - Turnera</BoldText>
    </MyTextView>
    <MyTextView>
      Podobne zmiany w okolicy lędźwiowej oraz na powierzchniach bocznych brzucha, zwłaszcza po
      stronie lewej. Podobnie jak objaw Cullena, występuje bardzo rzadko w ciężkim zapaleniu
      trzustki
    </MyTextView>
    <MyTextView>
      <BoldText>Objaw Goldflama</BoldText>
    </MyTextView>
    <MyTextView>
      Ostry ból pojawiający się po uderzeniu w okolicę kąta kręgosłupowo - żebrowego wskazujący na
      toczący się proces zapalny nerki po stronie badanej.
    </MyTextView>
    <MyTextView>
      <BoldText>Punkt Basy&apos;ego</BoldText>
    </MyTextView>
    <MyTextView>
      2–3 palce na zewnątrz od pępka w poziomej linii przechodzącej przez środek pępka. Może
      wykazywać bolesność uciskową w przypadku chorób nerek.
    </MyTextView>
    <MyTextView>
      <BoldText>Punkt Lanza</BoldText>
    </MyTextView>
    <MyTextView>
      Orientacyjny punkt na skórze brzucha odpowiadający położeniu wyrostka robaczkowego. Znajduje
      się na linii łączącej kolce biodrowe przednie górne, w jednej trzeciej odległości między nimi,
      po stronie prawej.
    </MyTextView>
    <MyTextView>
      <BoldText>Punkt McBurney&apos;a</BoldText>
    </MyTextView>
    <MyTextView>
      Zlokalizowany jest między przednim górnym kolcem biodrowym a pępkiem (ok.1/3 tego odcinka), w
      punkcie tym rzutuje się miejsce odejścia wyrostka od kątnicy. Stanowi miejsce największej
      bolesności w czasie zapalenia wyrostka robaczkowego.
    </MyTextView>
  </WskazowkiContainer>
);

export default ObjawyBrzuszne;
