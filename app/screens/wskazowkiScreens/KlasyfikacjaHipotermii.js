import React from 'react';
import { Image } from 'react-native';
import { MyTextView, BoldText } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

const KlasyfikacjaHipotermii = () => (
  <WskazowkiContainer>
    <Image source={require('./images/stetoskop.png')} style={styles.image} resizeMode="contain" />
    <BoldText>Stopień I</BoldText>
    <MyTextView>Objawy: przytomny, dreszcze. Temperatura 35 - 32 C.</MyTextView>
    <BoldText>Stopień II</BoldText>
    <MyTextView>
      Objawy: podsypiający, zaburzenia koordynacji ruchów, brak dreszczy. Temperatura 32 - 28 C.
    </MyTextView>
    <BoldText>Stopień III</BoldText>
    <MyTextView>
      Objawy: Nieprzytomny, możliwe zaburzenia rytmu serca, wolne, nitkowate tętno, rzadkie, płytkie
      oddechy. Temperatura 28 - 24 C.
    </MyTextView>
    <BoldText>Stopień IV</BoldText>
    <MyTextView>Objawy: Zatrzymanie krążenia. Temperatura poniżej 24 C.</MyTextView>
    <BoldText>Stopień V</BoldText>
    <MyTextView>Objawy: śmierć, zamarznięcie.</MyTextView>
  </WskazowkiContainer>
);

export default KlasyfikacjaHipotermii;
