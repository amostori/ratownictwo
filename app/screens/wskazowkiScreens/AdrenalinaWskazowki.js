import React from 'react';
import { Image } from 'react-native';
import { MyTextView, Sol } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

const AdrenalinaWskazowki = () => (
  <WskazowkiContainer>
    <Image source={require('./images/kid.png')} style={styles.image} resizeMode="contain" />
    <MyTextView>Dawka Adrenaliny u dzieci to 0,01 mg/kg.</MyTextView>
    <MyTextView>
      U świeżorodka i niemowląt najdokładniej i najwygodniej jest dawkować Adrenalinę dodając 1 mg
      tego leku do 100 ml
      <Sol />
    </MyTextView>
    <MyTextView>
      Następnie podaje się 1 ml/kg tego roztworu i.v. lub i.o. Będzie to równoważne dawce 0,01
      mg/kg.
    </MyTextView>
    <MyTextView>
      U starszych dzieci rozcieńcz 1 mg adrenaliny do 10 ml
      <Sol />
i podawaj po 0,1 ml/kg, czyli w 1 ml roztworu jest dawka na 10 kg wagi ciała.
    </MyTextView>
  </WskazowkiContainer>
);

export default AdrenalinaWskazowki;
