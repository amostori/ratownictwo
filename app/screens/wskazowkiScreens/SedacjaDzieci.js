import { Image } from 'react-native';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  MyTextView, BoldText, Hypertekst, ListaTextView, Sol,
} from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

class SedacjaDzieci extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleFentanyl = () => {
    const { navigation } = this.props;
    navigation.navigate('Fentanyl');
  };

  handleMidazolm = () => {
    const { navigation } = this.props;
    navigation.navigate('Midazolam');
  };

  render() {
    return (
      <WskazowkiContainer>
        <Image source={require('./images/kid.png')} style={styles.image} resizeMode="contain" />
        <MyTextView>
          U dzieci sedację i analgezję można osiągnąć podając
          <Hypertekst onPress={this.handleFentanyl}>Fentanyl</Hypertekst>
          1&micro;g/kg i
          <Hypertekst onPress={this.handleMidazolm}>Midazolam</Hypertekst>
          0,05 mg/kg.
        </MyTextView>
        <MyTextView>Miareczkować do osiągnięcia efektu</MyTextView>
        <MyTextView>
          <BoldText>Przygotowanie leków</BoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Fentanyl</BoldText>
        </MyTextView>
        <MyTextView>
          Rozcieńcz ampułkę (0,1 mg) do 10 ml
          <Sol />
i podawaj 0,1 ml/kg, czyli w 1 ml jest dawka na 10 kg wagi ciała.
        </MyTextView>
        <MyTextView>
          <BoldText>Midazolam</BoldText>
        </MyTextView>
        <MyTextView>
          Rozcieńcz 5 mg w 10 ml
          <Sol />
i podawaj po 0,1 ml/kg, czyli w 1 ml jest dawka na 10 kg wagi ciała.
        </MyTextView>
        <MyTextView>
          Dawka całkowita Midazolamu u dzieci 6 m.ż - 5 r.ż wynosi 6 mg, u dzieci 6 r.ż. - 12 r.ż.
          wynosi 10 mg.
        </MyTextView>
        <MyTextView>
          U dzieci &gt;12 lat dawka początkowa 2 - 2,5 mg. W razie potrzeby podawaj po 1 mg. Dawka
          całkowita 7,5 mg.
        </MyTextView>
        <MyTextView>
          <BoldText>Procedura:</BoldText>
        </MyTextView>
        <ListaTextView>
          1. W miarę możliwości uzyskaj świadomą zgodę pacjenta - poinformuj o ryzyku.
        </ListaTextView>
        <ListaTextView>2. Podaj 40 - 60% tlen</ListaTextView>
        <ListaTextView>
          3. Przygotuj pełen monitoring: oddech, tętno, ciśnienie, saturacja, ekg.
        </ListaTextView>
        <ListaTextView>
          4. Przygotuj sprzęt do udrażniania dróg oddechowych i prowadzenia wentylacji.
        </ListaTextView>
        <ListaTextView>5. Podaj Fentanyl powoli dożylnie</ListaTextView>
        <ListaTextView>6. Podaj Midazolam</ListaTextView>
        <ListaTextView>
          7. Oceń efekt, w razie potrzeby zwiększ dawkę leków (zastosuj głębszą sedację).
        </ListaTextView>
        <ListaTextView>8. Wykonaj zabieg.</ListaTextView>
        <MyTextView />
      </WskazowkiContainer>
    );
  }
}

export default SedacjaDzieci;
