import React from 'react';
import { Image } from 'react-native';
import { MyTextView, BoldText } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

const Screen4H4T = () => (
  <WskazowkiContainer>
    <Image source={require('./images/stetoskop.png')} style={styles.image} resizeMode="contain" />
    <MyTextView>
      W trakcie resuscytacji szukaj odwracalnych przyczyn zatrzymania krążenia. Zwykle jest to
      jedyny sposób na uratowanie pacjenta, który ma NZK w mechaniźmie PEA lub asystolii.
    </MyTextView>
    <MyTextView>
      <BoldText>4H:</BoldText>
      hipoksja, hipowolemia, hipotermia, zaburzenia elektrolitowe (hipo, hiper kalcemia, kaliemia
      lub glikemia).
      <BoldText>4T</BoldText>
      trombocytopenia (zaburzenia zakrzepowo - zatorowe), odma prężna (tension pneumothorax),
      zatrucia (toxins), tamponada osierdzia.
    </MyTextView>
  </WskazowkiContainer>
);

export default Screen4H4T;
