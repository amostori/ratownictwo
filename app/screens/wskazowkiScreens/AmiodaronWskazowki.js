import React from 'react';
import { Image } from 'react-native';
import { MyTextView, BoldText } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

const AmiodaronWskazowki = () => (
  <WskazowkiContainer>
    <Image source={require('./images/kid.png')} style={styles.image} resizeMode="contain" />
    <MyTextView>Dawka Amiodaronu u dzieci to 5 mg/kg.</MyTextView>
    <MyTextView>
      <BoldText>Dziecko &lt;21 kg (7 lat):</BoldText>
    </MyTextView>
    <MyTextView>
      Nabierz 2 ml (uwaga, w ampułce są 3 ml), czyli 100 mg Amiodaronu i rozcieńcz do 20 ml 5%
      glukozą. Teraz podawaj w dawce 1 ml/kg.
    </MyTextView>
    <MyTextView>
      <BoldText>Dziecko 20 kg (6 lat) - 30 kg (9 lat):</BoldText>
    </MyTextView>
    <MyTextView>
      Nabierz 1 ampułkę do strzykawki 20 ml i rozcieńcz do
      <BoldText>15 ml</BoldText>
      5% glukozą. Teraz podawaj po 0,5 ml/kg (połowa wagi dziecka).
    </MyTextView>
    <MyTextView>
      <BoldText>Dziecko &gt;30 kg (9 lat):</BoldText>
    </MyTextView>
    <MyTextView>
      Nabierz 4 ml leku (w ampułce są 3 ml, musisz użyć drugiej) do strzykawki dwudziestki i
      rozcieńcz do 20 ml 5% glukozą. Teraz podawaj po 0,5 ml/kg (połowa wagi dziecka).
    </MyTextView>
  </WskazowkiContainer>
);

export default AmiodaronWskazowki;
