import React from 'react';
import { Image } from 'react-native';
import {
  MyTextView, BoldText, Sol, ListaTextView,
} from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

const LeczenieGoraczki = () => (
  <WskazowkiContainer>
    <Image source={require('./images/kid.png')} style={styles.image} resizeMode="contain" />
    <MyTextView>
      <BoldText>
        Uwaga, samo schładzanie fizyczne jest nieskuteczne, wywołuje dreszcze i może paradoksalnie
        zwiększyć gorączkę. Należy wcześniej podać lek przeciwgorączkowy.
      </BoldText>
    </MyTextView>
    <MyTextView>Dzieci 0 - 3 m.ż: Paracetamol czopek 50 mg p.r.</MyTextView>
    <MyTextView>Dziecko 4 m.ż: Paracetamol czopek 80 mg p.r.</MyTextView>
    <MyTextView>
      <BoldText>
        U dzieci &gt;4 m.ż. lek przeciwgorączkowy pierwszego rzutu to Ibuprofen 5 - 10 mg/kg.
      </BoldText>
    </MyTextView>
    <MyTextView>Dzieci 4 m.ż - 2 r.ż: czopek 60 mg p.r.</MyTextView>
    <MyTextView>Dzieci &gt;2 r.ż: czopek 125 mg p.r.</MyTextView>

    <MyTextView>
      <BoldText>
        Uwaga, u wszystkich chorych na ospę wietrzną lub odwodnionych Ibuprofen nie jest zalecany:
        podaj Paracetamol:
      </BoldText>
    </MyTextView>
    <MyTextView>
      <BoldText>Dawkowanie doodbytnicze:</BoldText>
    </MyTextView>
    <ListaTextView>0 - 3 m.ż: 50 mg</ListaTextView>
    <ListaTextView>3 m.ż - 1 r.ż: 80 mg</ListaTextView>
    <ListaTextView>2 r.ż - 3 r.ż: 125 mg</ListaTextView>
    <ListaTextView>4 r.ż. - 9 r.ż: 250 mg</ListaTextView>
    <ListaTextView>&gt;9 r.ż: 500 mg</ListaTextView>
    <MyTextView />
    <MyTextView>
      <BoldText>Dawkowanie dożylne</BoldText>
    </MyTextView>
    <MyTextView>
      <BoldText>Dzieci &lt;10 kg (1 r.ż):</BoldText>
      Pobierz z fiolki 7,5 mg/kg (tj. 0,75 ml/kg) i rozcieńcz w stosunku nie większym niż 1:10 w
      <Sol />
      lub 5% glukozie.
    </MyTextView>
    <MyTextView>
      <BoldText>Dzieci 10 - 50 kg:</BoldText>
      Podaj 15 mg/kg (tj. 1,5 ml/kg) z fiolki.
    </MyTextView>
    <MyTextView>
      <BoldText>Dzieci &gt;50 kg:</BoldText>
      Podaj 1 g leku we wlewie.
    </MyTextView>
    <MyTextView>
      <BoldText>Przetaczać w przeciągu 15 min!</BoldText>
    </MyTextView>
  </WskazowkiContainer>
);

export default LeczenieGoraczki;
