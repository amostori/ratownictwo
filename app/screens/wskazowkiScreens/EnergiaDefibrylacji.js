import React from 'react';
import { Image } from 'react-native';
import { MyTextView, BoldText } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

const EnergiaDefibrylacji = () => (
  <WskazowkiContainer>
    <Image source={require('./images/stetoskop.png')} style={styles.image} resizeMode="contain" />
    <MyTextView>
      Obecnie zalecana jest energia, którą wskazuje producent urządzenia. Jeśli nie jest znana to
      należy użyć energii
      <BoldText>150 - 200 J</BoldText>
      dla defibrylatora dwufazowego i
      <BoldText>360 J</BoldText>
      dla jednofazowego.
    </MyTextView>
    <MyTextView>
      Kolejne wyładowania mogą być energią stopniowo zwiększaną lub stałą. Stąd dla dwufazowych jest
      to
      <BoldText>150 - 360 J</BoldText>
a dla jednofazowych
      <BoldText>360 J.</BoldText>
    </MyTextView>
    <MyTextView>
      W przypadku dzieci energia defibrylacji wynosi
      <BoldText>4 J/kg.</BoldText>
      Wynik zaokrąglamy w górę.
    </MyTextView>
    <MyTextView>Elektrody pedriatyczne (średnica 4,5 cm) używamy u dzieci do 10 kg m.c.</MyTextView>
  </WskazowkiContainer>
);

export default EnergiaDefibrylacji;
