import React from 'react';
import { StatusBar } from 'react-native';
import PropTypes from 'prop-types';
import { Container } from '../components/container';
import { AllImagesPage } from '../components/Images/HomeImageButton';
import { VersionTextView } from '../components/TextViews';

const Home = ({ navigation }) => (
  // navigation przesyłamy jako props do AllImagesPage
  <Container>
    <StatusBar backgroundColor="red" translucent={false} barStyle="light-content" />
    <AllImagesPage navigation={navigation} />
    <VersionTextView>wersja: 2.3</VersionTextView>
  </Container>
);

Home.propTypes = {
  navigation: PropTypes.object,
};
export default Home;
