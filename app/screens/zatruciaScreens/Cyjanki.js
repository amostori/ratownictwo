import { Image } from 'react-native';
import React from 'react';

import { MyTextView, BoldText, VersionSzczeklikERC } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

const Cyjanki = (
  <WskazowkiContainer>
    <Image source={require('./images/toxic.png')} style={styles.image} resizeMode="contain" />
    <MyTextView>
      Zatrucie cyjankami może zdarzyć się w czasie pożaru w pomieszczeniach. Tworzywa sztuczne paląc
      się wydzielają cyjanek.
    </MyTextView>
    <MyTextView>
      <BoldText>Odtrutka:</BoldText>
      Hydroksykobalamina (preparat Cyanokit).
    </MyTextView>
    <MyTextView>Lek niedostępny w karetce.</MyTextView>
    <VersionSzczeklikERC />
  </WskazowkiContainer>
);

export default Cyjanki;
