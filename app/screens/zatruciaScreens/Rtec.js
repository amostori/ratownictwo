import { Image } from 'react-native';
import React from 'react';

import { MyTextView, VersionSzczeklikERC } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

const Rtec = (
  <WskazowkiContainer>
    <Image source={require('./images/toxic.png')} style={styles.image} resizeMode="contain" />
    <MyTextView>
      Doustne spożycie rtęci jest nieszkodliwe. Prowokacja wymiotów czy płukanie żołądka nie jest
      konieczne.
    </MyTextView>
    <MyTextView>Szkodliwe natomiast jest wdychanie rtęci.</MyTextView>
    <VersionSzczeklikERC />
  </WskazowkiContainer>
);

export default Rtec;
