import { Image } from 'react-native';
import React from 'react';

import { MyTextView, BoldText, VersionSzczeklikERC } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

const Digoksyna = (
  <WskazowkiContainer>
    <Image source={require('./images/toxic.png')} style={styles.image} resizeMode="contain" />
    <MyTextView>
      Objawy: zaburzenia rytmu serca, zaburzenia widzenia - &quot;żółte halo&quot;.
    </MyTextView>
    <MyTextView>
      Rośliny zawierające glikozydy nasercowe: konwalia, oleander, cis, naparstnica, miłek wiosenny.
    </MyTextView>
    <MyTextView>
      <BoldText>Odtrutka:</BoldText>
      przeciwciała FAB
    </MyTextView>
    <MyTextView>Lek niedostępny w karetce.</MyTextView>
    <VersionSzczeklikERC />
  </WskazowkiContainer>
);

export default Digoksyna;
