import { Image } from 'react-native';
import React from 'react';

import { MyTextView, BoldText, VersionSzczeklikERC } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

const ZatrucieParacetamolem = (
  <WskazowkiContainer>
    <Image source={require('./images/toxic.png')} style={styles.image} resizeMode="contain" />
    <MyTextView>
      Objawy: żółtaczka i nudności pojawiające się po kilku - kilkunastu godzinach.
    </MyTextView>
    <MyTextView>Dawka toksyczna: 150 mg/kg.</MyTextView>
    <MyTextView>
      <BoldText>Odtrutka:</BoldText>
N - Acetylocysteina (preparat ACC)
    </MyTextView>
    <MyTextView>Lek niedostępny w karetce.</MyTextView>
    <VersionSzczeklikERC />
  </WskazowkiContainer>
);

export default ZatrucieParacetamolem;
