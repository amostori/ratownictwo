import { Image } from 'react-native';
import React from 'react';

import { MyTextView, BoldText, VersionSzczeklikERC } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

const Metanol = (
  <WskazowkiContainer>
    <Image source={require('./images/toxic.png')} style={styles.image} resizeMode="contain" />
    <MyTextView>Glikol etylenowy znajduje się w płynie chłodniczym, płynie hamulcowym.</MyTextView>
    <MyTextView>
      <BoldText>Uwaga, działa bardzo szybko!</BoldText>
    </MyTextView>
    <MyTextView>Podaj do wypicia 100 ml wódki rozcieńczonej wodą w stosunku 1:1.</MyTextView>
    <MyTextView>W przypadku zatrucia metanolem zdążysz przewieźć pacjenta do SOR.</MyTextView>
    <VersionSzczeklikERC />
  </WskazowkiContainer>
);

export default Metanol;
