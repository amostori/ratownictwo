import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollContainer } from '../../components/container';
import {
  MyTextView,
  Hypertekst,
  BoldText,
  ListaTextView,
  TiM,
  VersionTextView,
} from '../../components/TextViews';

export default class HipoglikemiaP extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleGlukagon = () => {
    const { navigation } = this.props;
    navigation.navigate('Glukagon');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <BoldText>Glukoza 20%</BoldText>
1 - 2,5 ml/kg (0,2 - 0,5 g/kg) i.v.
        </MyTextView>
        <MyTextView>
          Jeśli brak dojścia dożylnego podaj
          <Hypertekst onPress={this.handleGlukagon}>Glukagon:</Hypertekst>
        </MyTextView>
        <MyTextView>Dzieci &lt;25 kg - 0,5 mg i.m.</MyTextView>
        <MyTextView>Dzieci &gt;25 kg (zwykle &gt;8 lat) - 1 mg i.m.</MyTextView>
        <ListaTextView>
          Glukagon nie zadziała u noworodków oraz gdy hipoglikemii towarzyszy insulinoma, guz
          chromochłonny lub przewlekła choroba wątroby.
        </ListaTextView>
        <MyTextView />
        <TiM />
        <VersionTextView>
          Na podstawie wytycznych International Society for Pediatric and Adolescent Diabetes
          Clinical Practice Consensus Guidelines 2014.
        </VersionTextView>
      </ScrollContainer>
    );
  }
}
