import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { ButtonsYesNo } from '../../../components/Buttons';
import { MyTextView } from '../../../components/TextViews';
import Opony from './Opony';
import NieOpony from './NieOpony';

export default class NieSepsa extends React.Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  render() {
    const { divy } = this.state;
    const { navigation } = this.props;

    return (
      <View>
        <MyTextView>
          Sztywność karku, tętniące ciemiączko, zaburzenia świadomości, stan padaczkowy?
        </MyTextView>
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(<Opony navigation={navigation} />)}
          onPressNo={() => this.handleOnClick(<NieOpony navigation={navigation} />)}
        />
        <MyTextView />
        {divy}
      </View>
    );
  }
}
