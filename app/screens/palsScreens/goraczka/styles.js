import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  image: {
    width: 350,
  },
  imagesContainer: {
    flex: 1,
    alignItems: 'center',
  },
});

export default styles;
