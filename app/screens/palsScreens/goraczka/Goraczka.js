import React from 'react';
import PropTypes from 'prop-types';
import { Image, View, Linking } from 'react-native';
import { ButtonsYesNo } from '../../../components/Buttons';
import { MyTextView, Hypertekst } from '../../../components/TextViews';
import { ScrollContainer } from '../../../components/container';
import styles from './styles';
import Sepsa from './Sepsa';
import NieSepsa from './NieSepsa';

export default class Goraczka extends React.Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  handleSztywnoscKarku = () => {
    Linking.openURL('https://www.youtube.com/watch?v=bZY5bPyNDvI');
  };

  render() {
    const { divy } = this.state;
    const { navigation } = this.props;

    return (
      <ScrollContainer>
        <MyTextView>Podaj tlen aby utrzymać saturację 94 - 98 %.</MyTextView>
        <MyTextView>
          Nieblednąca wysypka (zwłaszcza o średnicy &gt;2 mm),
          <Hypertekst onPress={this.handleSztywnoscKarku}>sztywność karku</Hypertekst>
, dziecko
          septyczne, nawrót włośniczkowy &gt;3 sek?
        </MyTextView>
        <View style={styles.imagesContainer}>
          <Image
            source={require('./images/wysypka.jpg')}
            style={styles.image}
            resizeMode="contain"
          />

          <Image
            source={require('./images/wysypka2.jpg')}
            style={styles.image}
            resizeMode="contain"
          />
        </View>
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(<Sepsa navigation={navigation} />)}
          onPressNo={() => this.handleOnClick(<NieSepsa navigation={navigation} />)}
        />
        <MyTextView />
        {divy}
      </ScrollContainer>
    );
  }
}
