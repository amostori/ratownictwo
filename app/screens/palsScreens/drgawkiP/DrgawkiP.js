import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { ScrollContainer } from '../../../components/container';
import {
  MyTextView,
  Hypertekst,
  AskTextView,
  ListaTextView,
  BoldText,
  TiM,
  Sol,
  VersionTkaczyk,
} from '../../../components/TextViews';
import { ButtonsYesNo } from '../../../components/Buttons';

export default class DrgawkiP extends Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleDiazepam = () => {
    const { navigation } = this.props;
    navigation.navigate('DiazepamDzieci');
  };

  handleKlonazepam = () => {
    const { navigation } = this.props;
    navigation.navigate('Clonazepam');
  };

  handleMidazolam = () => {
    const { navigation } = this.props;
    navigation.navigate('MidazolamDzieci');
  };

  handleGoraczka = () => {
    const { navigation } = this.props;
    navigation.navigate('LeczenieGoraczki');
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  render() {
    const { divy } = this.state;
    const Wklucie = (
      <View>
        <MyTextView>
          <Hypertekst onPress={this.handleDiazepam}>Diazepam (Relanium)</Hypertekst>
          0,25 mg/kg iv
        </MyTextView>
        <MyTextView>lub</MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleKlonazepam}>Klonazepam</Hypertekst>
          0,5 mg w 5 ml
          <Sol />
        </MyTextView>
        <MyTextView>lub</MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleMidazolam}>Midazolam</Hypertekst>
          0,1 mg/kg iv.
        </MyTextView>
        <MyTextView>Jeśli do 5 minut brak efektu powtórz tę samą dawkę jeden raz.</MyTextView>
        <MyTextView>
          W przypadku drgawek gorączkowych
          <Hypertekst onPress={this.handleGoraczka}>obniżaj temperaturę.</Hypertekst>
        </MyTextView>
        <TiM />
        <VersionTkaczyk />
      </View>
    );

    const NoWklucie = (
      <View>
        <MyTextView>
          <Hypertekst onPress={this.handleDiazepam}>Diazepam</Hypertekst>
          0,5 mg/kg p.r. u dzieci do 10 kg, 5 mg u dzieci 10 - 15 kg i 10 mg u dzieci &gt;15 kg.
        </MyTextView>
        <MyTextView>lub</MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleMidazolam}>Midazolam</Hypertekst>
          podać cienką kaniulą (np. z wenflona) między policzek a dziąsło (uwaga, metoda
          niezarejestrowana w Polsce).
        </MyTextView>
        <MyTextView>
          <BoldText>Dawkowanie dopoliczkowo:</BoldText>
        </MyTextView>
        <ListaTextView>&lt;1 r.ż: 2,5 mg</ListaTextView>
        <ListaTextView>1 - 4 r.ż: 5 mg</ListaTextView>
        <ListaTextView>5 - 9 r.ż: 7,5 mg</ListaTextView>
        <ListaTextView>&gt;9 lat: 10 mg</ListaTextView>
        <MyTextView />
        <MyTextView>
          W przypadku drgawek gorączkowych
          <Hypertekst onPress={this.handleGoraczka}>obniżaj temperaturę.</Hypertekst>
        </MyTextView>
        <TiM />
        <VersionTkaczyk />
      </View>
    );
    return (
      <ScrollContainer>
        <MyTextView>Wyklucz hipoglikemię.</MyTextView>
        <AskTextView>Dojście dożylne obecne?</AskTextView>
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(Wklucie)}
          onPressNo={() => this.handleOnClick(NoWklucie)}
        />
        <MyTextView />
        {divy}
      </ScrollContainer>
    );
  }
}
