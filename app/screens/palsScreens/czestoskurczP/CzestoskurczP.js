import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Linking } from 'react-native';
import { ScrollContainer } from '../../../components/container';
import {
  MyTextView,
  Hypertekst,
  AskTextView,
  ListaTextView,
  BoldText,
  TiM,
  VersionERC,
} from '../../../components/TextViews';
import { ButtonsYesNo } from '../../../components/Buttons';

export default class CzestoskurczP extends Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleSedacja = () => {
    const { navigation } = this.props;
    navigation.navigate('SedacjaDzieci');
  };

  handleAmiodaron = () => {
    const { navigation } = this.props;
    navigation.navigate('AmiodaronWskazowki');
  };

  handleKardiowersja = () => {
    Linking.openURL('https://www.youtube.com/watch?v=1fjmyog37Fo');
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  render() {
    const { divy } = this.state;
    const Wystepuja = (
      <View>
        <MyTextView>
          <BoldText>NIESTABILNY</BoldText>
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleSedacja}>Sedacja i analgezja.</Hypertekst>
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleKardiowersja}>Kardiowersja</Hypertekst>
- pierwsze
          wyładowanie 1 J/kg, drugie 2 J/kg.
        </MyTextView>
        <MyTextView>
          Jeśli nieskuteczne:
          <Hypertekst onPress={this.handleAmiodaron}>Amiodaron</Hypertekst>
5 mg/kg i.v. i kolejna
          kardiowersja 2 J/kg.
        </MyTextView>
        <TiM />
        <VersionERC />
      </View>
    );

    const NieWystepuja = (
      <View>
        <MyTextView>
          <BoldText>STABILNY</BoldText>
        </MyTextView>
        <TiM />
        <VersionERC />
      </View>
    );
    return (
      <ScrollContainer>
        <AskTextView>Czy występują objawy niepokojące?</AskTextView>
        <ListaTextView>(wstrząs, zaburzenia świadomości?)</ListaTextView>
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(Wystepuja)}
          onPressNo={() => this.handleOnClick(NieWystepuja)}
        />
        <MyTextView />
        {divy}
      </ScrollContainer>
    );
  }
}
