import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollContainer } from '../../components/container';
import {
  MyTextView,
  Hypertekst,
  VersionTkaczyk,
  BoldText,
  ListaTextView,
  Sol,
  TiM,
} from '../../components/TextViews';

export default class BolBrzuchaP extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleObjawyBrzuszne = () => {
    const { navigation } = this.props;
    navigation.navigate('ObjawyBrzuszne');
  };

  handleMorfina = () => {
    const { navigation } = this.props;
    navigation.navigate('MorfinaDzieci');
  };

  handleSkalaBolu = () => {
    const { navigation } = this.props;
    navigation.navigate('SkalaBolu');
  };

  handleParacetamol = () => {
    const { navigation } = this.props;
    navigation.navigate('Paracetamol');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <Hypertekst onPress={this.handleObjawyBrzuszne}>Objawy patologiczne</Hypertekst>
        </MyTextView>
        <MyTextView>
          Jeśli ból wymaga zniesienia (zwykle &gt;7 punktów w
          <Hypertekst onPress={this.handleSkalaBolu}>skali bólu</Hypertekst>
) podaj
          <Hypertekst onPress={this.handleParacetamol}>Paracetamol:</Hypertekst>
        </MyTextView>
        <MyTextView>
          <BoldText>Dzieci &gt;12 lat:</BoldText>
          500 mg p.o.
        </MyTextView>
        <MyTextView>
          <BoldText>Dzieci &lt;12 lat</BoldText>
          15 mg/kg p.o. lub p.r.
        </MyTextView>
        <MyTextView>
          <BoldText>Dawkowanie doodbytnicze:</BoldText>
        </MyTextView>
        <ListaTextView>0 - 3 m.ż: 50 mg</ListaTextView>
        <ListaTextView>3 m.ż - 1 r.ż: 80 mg</ListaTextView>
        <ListaTextView>2 r.ż - 3 r.ż: 125 mg</ListaTextView>
        <ListaTextView>4 r.ż. - 9 r.ż: 250 mg</ListaTextView>
        <ListaTextView>&gt;9 r.ż: 500 mg</ListaTextView>
        <MyTextView />
        <MyTextView>
          <BoldText>Dawkowanie dożylne</BoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dzieci &lt;10 kg (1 r.ż):</BoldText>
          Pobierz z fiolki 7,5 mg/kg (tj. 0,75 ml/kg) i rozcieńcz w stosunku nie większym niż 1:10 w
          <Sol />
          lub 5% glukozie.
        </MyTextView>
        <MyTextView>
          <BoldText>Dzieci 10 - 50 kg:</BoldText>
          Podaj 15 mg/kg (tj. 1,5 ml/kg) z fiolki.
        </MyTextView>
        <MyTextView>
          <BoldText>Dzieci &gt;50 kg:</BoldText>
          Podaj 1 g leku we wlewie.
        </MyTextView>
        <MyTextView>
          <BoldText>Uwaga, przetaczać w przeciągu 15 min!</BoldText>
        </MyTextView>
        <MyTextView>
          W przypadku ostrego brzucha, zapalenia otrzewnej, silnej kolki nerkowej ból może nie
          ustąpić. W takim przypadku podaj
          <Hypertekst onPress={this.handleMorfina}>Morfinę</Hypertekst>
          0,1 mg/kg i.v.
        </MyTextView>
        <MyTextView>Morfiny nie podawaj w przypadku kolki wątrobowej.</MyTextView>
        <TiM />
        <VersionTkaczyk />
      </ScrollContainer>
    );
  }
}
