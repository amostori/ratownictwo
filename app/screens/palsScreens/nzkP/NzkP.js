import React from 'react';
import PropTypes from 'prop-types';
import { ButtonsYesNo } from '../../../components/Buttons';
import { MyTextView, AskTextView } from '../../../components/TextViews';
import { ScrollContainer } from '../../../components/container';
import Defi from './Defi';
import NoDefi from './NoDefi';

export default class NzkP extends React.Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  render() {
    const { divy } = this.state;
    const { navigation } = this.props;

    return (
      <ScrollContainer>
        <MyTextView>Oceń rytm serca najszybciej jak to możliwe.</MyTextView>
        <MyTextView>
          Do czasu przyłożenia łyżek defibrylatora prowadź resuscytację krążeniowo oddechową w
          stosunku 15:2 (zacznij od 5 wdmuchnięć).
        </MyTextView>
        <AskTextView>Rytm do defibrylacji?</AskTextView>
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(<Defi navigation={navigation} />)}
          onPressNo={() => this.handleOnClick(<NoDefi navigation={navigation} />)}
        />
        <MyTextView />
        {divy}
      </ScrollContainer>
    );
  }
}
